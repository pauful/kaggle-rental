import pandas as pd
import sys
import numpy as np
from collections import defaultdict

pd_train = pd.read_json('../data/train-formatted.json')
pd_train = pd_train[pd.notnull(pd_train['features'])]
pd_train['interest_level'] = pd.factorize(pd_train['interest_level'])[0]
header = ['features','interest_level']
pd_train.to_csv('train.tsv', columns=header, sep='\t', encoding='utf-8')
