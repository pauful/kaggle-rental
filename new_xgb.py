#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :
import gpxpy.geo
import time, datetime, json
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics import  make_scorer
from  sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import StratifiedKFold
from nltk.stem.porter import *
from scipy.spatial.distance import euclidean
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata
from sklearn.feature_selection import SelectKBest, f_classif, f_regression
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import xgboost as xgb
import sklearn.utils
from sklearn.utils import check_array, check_consistent_length
from sklearn.decomposition import PCA

import sys
reload(sys)
sys.setdefaultencoding('utf8')
def _weighted_sum(sample_score, sample_weight, normalize=False):
    if normalize:
        return np.average(sample_score, weights=sample_weight)
    elif sample_weight is not None:
        return np.dot(sample_score, sample_weight)
    else:
        return sample_score.sum()

def log_loss(y_true, y_pred, eps=1e-15, normalize=True, sample_weight=None,
             labels=None):

    y_pred = check_array(y_pred, ensure_2d=False)
    check_consistent_length(y_pred, y_true)

    lb = LabelBinarizer()

    if labels is not None:
        lb.fit(labels)
    else:
        lb.fit(y_true)

    if len(lb.classes_) == 1:
        if labels is None:
            raise ValueError('y_true contains only one label ({0}). Please '
                             'provide the true labels explicitly through the '
                             'labels argument.'.format(lb.classes_[0]))
        else:
            raise ValueError('The labels array needs to contain at least two '
                             'labels for log_loss, '
                             'got {0}.'.format(lb.classes_))

    transformed_labels = lb.transform(y_true)

    if transformed_labels.shape[1] == 1:
        transformed_labels = np.append(1 - transformed_labels,
                                       transformed_labels, axis=1)

    # Clipping
    y_pred = np.clip(y_pred, eps, 1 - eps)

    # If y_pred is of single dimension, assume y_true to be binary
    # and then check.
    if y_pred.ndim == 1:
        y_pred = y_pred[:, np.newaxis]
    # print( y_pred.shape)
    if y_pred.shape[1] == 1:
        y_pred = np.append(1 - y_pred, y_pred, axis=1)

    # Check if dimensions are consistent.
    transformed_labels = check_array(transformed_labels)
    if len(lb.classes_) != y_pred.shape[1]:
        if labels is None:
            raise ValueError("y_true and y_pred contain different number of "
                             "classes {0}, {1}. Please provide the true "
                             "labels explicitly through the labels argument. "
                             "Classes found in "
                             "y_true: {2}".format(transformed_labels.shape[1],
                                                  y_pred.shape[1],
                                                  lb.classes_))
        else:
            raise ValueError('The number of classes in labels is different '
                             'from that in y_pred. Classes found in '
                             'labels: {0}'.format(lb.classes_))

    # Renormalize

    y_pred /= y_pred.sum(axis=1)[:, np.newaxis]

    loss = -(transformed_labels * np.log(y_pred)).sum(axis=1)



    return _weighted_sum(loss, sample_weight, normalize)


features_to_use = ['bathrooms', 'bedrooms', 'price', 'length_features', 'length_desc',
'pictures_length', 'year', 'month', 'day', 'weekday', 'price_be', 'price_ba',  'num_features', 'word_desc', 'word_features', 'prize_feature', 'prize_desc',
'price_be_ba', 'manager_id_enc', 'building_id_enc',
#'price_avg_manager',
'price_avg_building', 'manager_id_count', 'building_id_count',#'manager_id_count_more',
'pictures_length_avg_building', 'pictures_length_avg_manager',
'avg_pic_room',
# 'bed_&_bath','bed_or_bath',
'avg_length_desc', 'avg_length_features',
'latitude', 'longitude',
# 'listing_months','listing_days', 'listing_weeks',
'price_to_max_low', 'price_to_max_high', 'price_to_max_medium',
'hour','am','weekend',
'week','kmeans5','kmeans10','kmeans25','kmeans15','kmeans20','kmeans30',
'bkmeans5','bkmeans10','bkmeans25','bkmeans15','bkmeans20','bkmeans30',
'skmeans5','skmeans10','skmeans25','skmeans15','skmeans20','skmeans30',
'wkmeans5','wkmeans10','wkmeans25','wkmeans15','wkmeans20','wkmeans30',
# 'distance_to_center5_0','distance_to_center5_1','distance_to_center5_2','distance_to_center5_3','distance_to_center5_4',

# 'colkmeans5','colkmeans10','colkmeans25','colkmeans15','colkmeans20','colkmeans30',

# 'varkmeans5','varkmeans10','varkmeans15','varkmeans20','varkmeans25','varkmeans30',
#'pkmeans5','pkmeans10','pkmeans25','pkmeans15','pkmeans20','pkmeans30',
# 'tkmeans5','tkmeans10','tkmeans25','tkmeans15','tkmeans20',
# 'neighbours', 'bneighbours',
 'exclamation', 'interrognat', #'interrognat_exc',
 # 'lda10','lda20','lda30','lda40','lda50','lda80','lda100',
 # 'des_kmeans2_5','des_kmeans2_10','des_kmeans2_15','des_kmeans2_20','des_kmeans2_25',
 # 'des_kmeans3_5','des_kmeans3_10','des_kmeans3_15','des_kmeans3_20','des_kmeans3_25',
 # 'des_kmeans5_5','des_kmeans5_10','des_kmeans5_15','des_kmeans5_20','des_kmeans5_25',
 # 'des_kmeans_5','des_kmeans_10','des_kmeans_15','des_kmeans_20','des_kmeans_25',

 # 'price_to_max_low_mean', 'price_to_max_high_mean', 'price_to_max_medium_mean',
 # 'price_to_max_low_median', 'price_to_max_high_median', 'price_to_max_medium_median']
# 'img_size_to_max_low',
#'img_size_to_max_high', 'img_size_to_max_medium',
# 'brightness_to_max_low',

#'address_enc']
'address_enc_2',
'price_week','price_month','price_day',
# 'avg_price_week', 'avg_price_month', 'avg_price_day',
'street_address_enc_2',
'brightness_to_max_high', 'brightness_to_max_medium', 'size_img','brightness',
'manager_id_medium','manager_id_high']
# 'bed_list_avg', 'bed_list_avg_price',
# 'bed_list_avg_int_h','bed_list_avg_int_m']
# 'mgr_list_avg','building_list_avg',
# 'building_list_avg_price']
 # 'price_avg_manager_time',
 # 'high_mgr_list_avg','medium_mgr_list_avg', 'high_mgr_list_avg_price','medium_mgr_list_avg_price']
 # 'ikmeans_low_5','ikmeans_low_10','ikmeans_low_20','ikmeans_low_15','ikmeans_low_25','ikmeans_low_30',
 # 'ikmeans_medium_5','ikmeans_medium_10','ikmeans_medium_20','ikmeans_medium_15','ikmeans_medium_25','ikmeans_medium_30',
 # 'ikmeans_high_5','ikmeans_high_10','ikmeans_high_20','ikmeans_high_15','ikmeans_high_25','ikmeans_high_30']

# 'manager_id_medium', 'manager_id_high',
# 'building_id_medium', 'building_id_high']
#, 'manager_id_low']
# 'brightness_price']

class cust_regression_vals(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, hd_searches):

        array_of_features_to_use = features_to_use  #'year',

        hd_searches = hd_searches[array_of_features_to_use]
        return hd_searches


class cust_txt_col(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key
    def fit(self, x, y=None):
        return self
    def transform(self, data_dict):
        return data_dict[self.key].apply(str)

class to_array(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, data_dict):
        return data_dict.toarray()

starting_time = datetime.datetime.now()
print("Starting time")
print(str(starting_time))

def get_shops(filename, fieldname):
    pd_train = pd.read_csv(filename)
    pd_train = pd_train.dropna()
    l = []
    for c in pd_train[fieldname].tolist():
        if len(c.split('(')) > 1:
            l.append(c.split('(')[1].split(')')[0])
    # pd_train['place'] = pd_train['Location'].map(lambda x: x.split('(')[1].split(')')[0])

    # l= pd_train['place'].tolist()
    toret = []
    for i in l:
        try:
            toret.append((float(i.split(',')[1]),float(i.split(',')[0])))
        except:
            pass
    return toret

pd_train = pd.read_json('data/train-formatted.json')
pd_test = pd.read_json('data/test-formatted.json')
id_test = pd_test['listing_id']



num_train = pd_train.shape[0]
df_all = pd.concat((pd_train, pd_test), axis=0, ignore_index=True)

# df_all.loc[(df_all['bedrooms'] == 0) & df_all['description'].str.contains('bed') ,'bedrooms']=1


def change_street(x):

    x = x.lower()
    x = x.replace('st.', 'street')
    x = x.replace('st,', 'street')


def change_features(x):

    if not isinstance(x, basestring):
        return ''

    x = x.lower()

    x = remove_html(x)
    x = x.strip(' ')
    x = x.strip('\t')
    x = x.strip('\n')
    x = x.strip('\r')
    x = re.sub(r'\d+', ' ', x)

    x = x.replace('/', ' ')
    x = x.replace('!', ' ')
    x = x.replace('(', ' ')
    x = x.replace(')', ' ')
    x = x.replace('...', ' ')
    x = x.replace('..', ' ')
    x = x.replace('"', ' ')
    x = x.replace('\'', '')
    x = x.replace('___', ' ')
    x = x.replace('xlarge', 'extra large')


    x = x.replace('pre war', 'prewar')
    x = x.replace('pre-war', 'prewar')

    x = x.replace('post war', 'postwar')
    x = x.replace('post-war', 'postwar')

    x = x.replace('dish washer', 'dishwasher')
    x = x.replace('fire place', 'fireplace')
    x = x.replace('walk in', 'walkin')
    x = x.replace('walk-in', 'walkin')
    x = x.replace('on site', 'onsite')
    x = x.replace('on-site', 'onsite')
    x = x.replace('counter-tops', 'countertops')
    x = x.replace('counter tops', 'countertops')
    x = x.replace('roof-deck', 'roofdeck')
    x = x.replace('roof deck', 'roofdeck')
    x = x.replace('eat in', 'eatin')
    x = x.replace('eat-in', 'eatin')
    x = x.replace('walk up', 'walkup')
    x = x.replace('over-size', 'oversize')
    x = x.replace('over size', 'oversize')
    x = x.replace('low-rise', 'lowrise')
    x = x.replace('low rise', 'lowrise')
    x = x.replace('doormen', 'doorman')
    x = x.replace('door man', 'doorman')
    x = x.replace('door men', 'doorman')
    x = x.replace('wat ', 'water ')
    x = x.replace('wash dryer', 'washer dryer')
    x = x.replace('washer & dryer', 'washer dryer')
    x = x.replace('washer and dryer', 'washer dryer')
    x = x.replace('wi fi', 'wifi')
    x = x.replace('wi-fi', 'wifi')
    x = x.replace('laundri', 'laundry')
    x = x.replace('laundri', 'laundry')

    x = x.replace('pets ok', 'pets allowed')
    x = x.replace('a c', 'air conditioning')

    x = x.replace('high ceilings', 'high ceiling')
    x = x.replace('outdoor areas', 'outdoor space')

    x = x.replace('wifi access', 'wifi')
    x = x.replace('washer in unit', 'washer')

    # x = (" ").join([stemmer.stem(z) for z in x.split(" ")])

    return x


from joblib import Parallel, delayed
import multiprocessing

# what are your inputs, and what operation do you want to

# perform on each input. For example...

inputs = range(10)

def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

def processInput(serie, coord):
    ll = {}
    for x in serie:
        lat = float(x.split('#')[0])
        lon = float(x.split('#')[1])
        ll[x]={}
        tt = []
        for c in coord:
            tt.append(gpxpy.geo.haversine_distance(lat, lon, c[1], c[0]))
        npa = np.asarray(tt)
        if len(npa) < 1:
            print npa
        ll[x]['min'] = np.min(npa)
        ll[x]['1000'] = len(np.where( npa < 1000 )[0])
        ll[x]['150'] = len(np.where( npa < 150 )[0])

    return ll

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def calculate_distances(serie, coord):

    num_cores = 8

    total = len(serie)/num_cores

    # lseries = [serie[:total], serie[total:total*2], serie[total*2:total*3], serie[total*3:total*4], serie[total*4:total*5], serie[total*5:total*6],serie[total*6:total*7],serie[total*7:]]

    print 'starting clacls'
    results = Parallel(n_jobs=8)(delayed(processInput)(i,coord) for i in chunks(serie, 1000))
    print 'all calculated'

    distances1 = merge_dicts(*results)

    return distances1

def distance_to_close_sub1(x, coord, distances):
    return float(distances[x]['min'])

def how_many_under_a_km(x, coord, distances):
    return float(distances[x]['1000'])

def how_many_under_a_150m(x, coord, distances):
    return float(distances[x]['150'])


CONST_LENGTH=1

def calc_features_aprox(x):
    if len(x)<1:
        return CONST_LENGTH
    else:
        return len(re.split(r"\~|;|,|\*|\.|\n", x))+1


def lenPics(x, sep):
    x = x.strip(' ')
    if len(x)<1:
        return CONST_LENGTH
    else:
        return len(x.split(sep))+1


def first_updates(df_all_i):

    df_all_i['length_features'] = df_all_i['features'].map(lambda x: len(x))
    df_all_i['length_desc'] = df_all_i['description'].map(lambda x: len(x))
    df_all_i['word_desc'] = df_all_i['description'].map(lambda x: lenPics(x, ' '))
    df_all_i['word_features'] = df_all_i['features'].map(lambda x: lenPics(x, ' '))
    df_all_i['prize_feature'] = df_all_i["price"]/df_all_i["word_features"]
    df_all_i['prize_desc'] = df_all_i["price"]/df_all_i["word_desc"]

    df_all_i['num_features'] = df_all_i['features'].map(lambda x: calc_features_aprox(x))

    df_all_i['year'] =   df_all_i['created'].map(lambda x: int(x.split(" ")[0].split("-")[0]))

    df_all_i['month'] =   df_all_i['created'].map(lambda x: int(x.split(" ")[0].split("-")[1]))

    df_all_i['day'] =   df_all_i['created'].map(lambda x: int(x.split(" ")[0].split("-")[2]))

    df_all_i['weekday'] =  df_all_i['created'].map(lambda x: datetime.datetime(int(x.split(" ")[0].split("-")[0]),int(x.split(" ")[0].split("-")[1]),int(x.split(" ")[0].split("-")[2]), 0 ,0 ,0).weekday())

    df_all_i['weekend'] = df_all_i['weekday'].map(lambda x: int(x>3))

    df_all_i['pictures_length'] =   df_all_i['photos'].map(lambda x: lenPics(x, ','))

    # df_all_i.loc[df_all_i['bedrooms']==0, 'bedrooms'] = 0.1
    # df_all_i.loc[df_all_i['bathrooms']==0, 'bathrooms'] = 0.1

    df_all_i["price_be"] = df_all_i["price"]/df_all["bedrooms"]
    df_all_i["price_ba"] = df_all_i["price"]/df_all["bathrooms"]

    df_all_i["price_be_ba"] = df_all_i["price"]/(df_all_i["bedrooms"]+df_all_i["bathrooms"])

    df_all_i["price_be_c"] = df_all_i["price_be"].map(lambda x: zeroIfnan(x,-1))


    df_all_i['price_sum'] = df_all_i['price'].groupby(df_all_i['building_id']).transform('sum')
    df_all_i['building_id_count'] = df_all_i['price'].groupby(df_all_i['building_id']).transform('count')
    df_all_i['price_avg_building'] = df_all_i['price_sum'] / df_all_i['building_id_count']

    df_all_i['building_list_avg_price'] = df_all_i['building_list_avg'] -df_all_i['price']

    df_all_i['bed_sum'] = df_all_i['bedrooms'].groupby(df_all_i['building_id']).transform('sum')
    df_all_i['bed_count'] = df_all_i['bedrooms'].groupby(df_all_i['building_id']).transform('count')
    df_all_i['bed_avg_building'] = df_all_i['bed_sum'] / df_all_i['bed_count']

    df_all_i['bath_sum'] = df_all_i['bathrooms'].groupby(df_all_i['building_id']).transform('sum')
    df_all_i['bath_count'] = df_all_i['bathrooms'].groupby(df_all_i['building_id']).transform('count')
    df_all_i['bath_avg_building'] = df_all_i['bath_sum'] / df_all_i['bath_count']


    df_all_i['price_sum'] = df_all_i['price'].groupby(df_all_i['manager_id']).transform('sum')
    df_all_i['price_count'] = df_all_i['price'].groupby(df_all_i['manager_id']).transform('count')
    df_all_i['price_avg_manager'] = df_all_i['price_sum'] / df_all_i['price_count']
    df_all_i['price_avg_manager_time'] = df_all_i['mgr_list_avg']/df_all_i['price']



    df_all_i['manager_id_count'] = df_all_i['price'].groupby(df_all_i['manager_id']).transform('count')
    df_all_i['manager_id_count_more'] = df_all_i['manager_id_count'].map(lambda x : int(x>1))

    df_all_i['pictures_length_sum'] = df_all_i['pictures_length'].groupby(df_all_i['manager_id']).transform('sum')
    df_all_i['pictures_length_count'] = df_all_i['pictures_length'].groupby(df_all_i['manager_id']).transform('count')
    df_all_i['pictures_length_avg_manager'] = df_all_i['pictures_length_sum'] / df_all_i['pictures_length_count']

    df_all_i['pictures_length_sum'] = df_all_i['pictures_length'].groupby(df_all_i['building_id']).transform('sum')
    df_all_i['pictures_length_count'] = df_all_i['pictures_length'].groupby(df_all_i['building_id']).transform('count')
    df_all_i['pictures_length_avg_building'] = df_all_i['pictures_length_sum'] / df_all_i['pictures_length_count']

    df_all_i['bed_list_avg_int_m'] = df_all_i['bed_list_avg_int_m'] / df_all_i['price']
    df_all_i['bed_list_avg_int_h'] = df_all_i['bed_list_avg_int_h'] / df_all_i['price']

    df_all_i['high_mgr_list_avg_price'] = df_all_i['high_mgr_list_avg']/df_all_i['price']
    df_all_i['medium_mgr_list_avg_price'] = df_all_i['medium_mgr_list_avg']/df_all_i['price']

    enc = LabelEncoder()
    ohenc = OneHotEncoder()
    df_all_i['building_id_enc'] = enc.fit_transform(df_all['building_id'])

    df_all_i['manager_id_enc'] = enc.fit_transform(df_all['manager_id'])

    # new_created = ['length_features', 'length_desc', 'word_desc', 'word_features', 'prize_feature', 'prize_desc', 'num_features', 'year', 'month', 'day', 'weekday', 'pictures_length', 'price_be', 'price_ba', 'price_be_ba',
    # 'price_sum', 'price_count', 'price_avg_building', 'bed_sum', 'bed_count', 'bed_avg_building', 'bath_avg_building', 'building_id_count', 'price_avg_manager', 'manager_id_count', 'pictures_length_avg_manager',
    # 'pictures_length_avg_building', 'building_id_enc', 'manager_id_enc', 'listing_id']
    #
    # df_all_i[new_created].to_csv('data/train_few_features.csv', encoding='utf-8', index=False)

    return df_all_i

def address_(x):
    x = x.lower()
    x = x.replace('.', '')
    x = x.replace(' street', ' ')
    x = x.replace(' st', ' ')
    x = x.replace(' sq', ' ')
    x = x.replace(' square', ' ')
    x = x.replace(' av', ' ')
    x = x.replace(' avenue', ' ')
    x = x.replace(' court', ' ')
    # x = x.replace('th', '')
    # x = x.replace('nd', '')
    x = x.replace(' uare', ' ')

    # x = x.replace('rd', '')
    x = x.replace(' ockholm', ' stockholm')



    x = x.replace(' enue', ' ')
    x = x.replace('-', '')
    x = x.replace(',', '')
    x = x.strip()
    return x





# df_all_new_features = first_updates(df_all)


import string
def remove_punctuation(x):
    if not isinstance(x, basestring):
        return ''
    for p in string.punctuation:
        x = x.replace(p, ' ')
    return x


import math
def new_round(df_round):

    df_round['avg_pic_room'] = df_round['pictures_length']/(df_round['bathrooms']+df_round['bedrooms'])
    # df_round['bed_&_bath'] = df_round['bathrooms']+df_round['bedrooms']
    # df_round['bed_or_bath'] = df_round['bedrooms']/(df_round['bathrooms']+df_round['bedrooms'])
    # df_round.loc[df_round['bed_&_bath']==np.inf, 'bed_&_bath'] = -1
    # df_round.loc[df_round['bed_or_bath']==np.inf, 'bed_or_bath'] = -1
    # df_round.loc[df_round['bed_&_bath']==np.nan, 'bed_&_bath'] = -1
    # df_round.loc[df_round['bed_or_bath']==np.nan, 'bed_or_bath'] = -1


    df_round.loc[df_round['avg_pic_room']==np.inf, 'avg_pic_room'] = -1
    # df_round['avg_pic_room'][df_round['avg_pic_room']==np.inf] = np.nan
    df_round['avg_length_features'] = df_round['length_features']/df_round['word_features']
    df_round['avg_length_desc'] = df_round['length_desc']/df_round['word_desc']



    df_round['week'] =  df_round['created'].map(lambda x: datetime.datetime(int(x.split(" ")[0].split("-")[0]),int(x.split(" ")[0].split("-")[1]),int(x.split(" ")[0].split("-")[2]), 0 ,0 ,0).isocalendar()[1])

    listing_months = df_round['listing_id'].groupby(df_round['month']).transform('count')
    listing_days = df_round['listing_id'].groupby(df_round['day']).transform('count')
    listing_weeks = df_round['listing_id'].groupby(df_round['week']).transform('count')

    price_months = df_round['price'].groupby(df_round['month']).transform('sum')
    price_days = df_round['price'].groupby(df_round['day']).transform('sum')
    price_weeks = df_round['price'].groupby(df_round['week']).transform('sum')

    avg_months = price_months/listing_months
    avg_weeks = price_weeks/listing_weeks
    avg_days = price_days/listing_days

    df_round['avg_price_week'] = df_round['week'].map(lambda x: avg_weeks[x])
    df_round['avg_price_month'] = df_round['month'].map(lambda x: avg_months[x])
    df_round['avg_price_day'] = df_round['day'].map(lambda x: avg_days[x])

    df_round['price_week'] = df_round['avg_price_week'] -df_round['price']
    df_round['price_month'] = df_round['avg_price_month'] -df_round['price']
    df_round['price_day'] = df_round['avg_price_day'] -df_round['price']


    # df_round['price'] = df_round['price'].map(lambda x: x*math.log(x))


    df_round_clean = df_round[df_round['price'] > 1000]
    median_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean.iloc[:num_train]['interest_level'])['price'].median()
    mean_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean.iloc[:num_train]['interest_level'])['price'].mean()
    count_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean.iloc[:num_train]['interest_level'])['price'].count()
    sum_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean.iloc[:num_train]['interest_level'])['price'].sum()
    avg_prices = sum_prices / count_prices

    size_img_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean['interest_level'])['size_img'].sum()
    avg_size_img = size_img_prices / count_prices
    brightness_img_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean['interest_level'])['brightness'].sum()
    avg_brightness = size_img_prices / count_prices

    # used_for_calc = df_round.iloc[:num_train]
    # avg_interest = used_for_calc['interest_level'].groupby(used_for_calc['interest_level'])['interest_level'].count()
    # print avg_interest
    # df_round['avg_interest_level'] = df_round['interest_level'].map(lambda x: avg_interest[x])

    # print count_prices
    # print sum_prices
    # print avg_prices

    df_round['img_size_to_max_low'] = avg_size_img['low']/df_round['price']
    df_round['img_size_to_max_high'] = avg_size_img['high']/df_round['price']
    df_round['img_size_to_max_medium'] = avg_size_img['medium']/df_round['price']

    df_round['brightness_to_max_low'] = avg_brightness['low']/df_round['price']
    df_round['brightness_to_max_high'] = avg_brightness['high']/df_round['price']
    df_round['brightness_to_max_medium'] = avg_brightness['medium']/df_round['price']

    df_round['brightness_price'] = df_round['brightness']/df_round['price']

    df_round['price_to_max_low'] = avg_prices['low']/df_round['price']
    df_round['price_to_max_high'] = avg_prices['high']/df_round['price']
    df_round['price_to_max_medium'] = avg_prices['medium']/df_round['price']


    df_round['price_to_max_low_mean'] = mean_prices['low']/df_round['price']
    df_round['price_to_max_high_mean'] = mean_prices['high']/df_round['price']
    df_round['price_to_max_medium_mean'] = mean_prices['medium']/df_round['price']


    df_round['price_to_max_low_median'] = median_prices['low']/df_round['price']
    df_round['price_to_max_high_median'] = median_prices['high']/df_round['price']
    df_round['price_to_max_medium_median'] = median_prices['medium']/df_round['price']

    df_round['bed_list_avg_price'] = df_round['bed_list_avg']/df_round['price']

    df_round['hour'] = df_round['created'].map(lambda x: int(x.split(" ")[1].split(":")[0]))
    def am(x):
        if x>=0 and x < 8:
            return 1
        elif x>=8 and x <16:
            return 2
        else:
             return 3
    df_round['am'] = df_round['hour'].map(lambda x: am(x))

    # print df_round[['created','hour','am','pm', 'interest_level']]


    df_round['exclamation'] = df_round['description'].map(lambda x: x.count('!'))
    df_round['interrognat'] = df_round['description'].map(lambda x: x.count('?'))
    df_round['interrognat_exc'] =df_round['interrognat'] + df_round['exclamation']

    enc = LabelEncoder()
    df_round['address_enc'] = enc.fit_transform(df_round['display_address_new'])

    return df_round

from sklearn import preprocessing

def zeroIfnan(x, val):
    if math.isnan(x):
        return val
    elif x == np.inf:
        return val
    elif x == np.nan:
        return val
    else:
        return x

def zeroIfnanS(x, val):
    if isinstance(x, basestring):
        return x
    else:
        return val

def getnansout(df, col, val):
    df.loc[df[col]==np.nan, col] =val
    df[col] = df[col].map(lambda x: zeroIfnan(x, val))
    df.loc[df[col]==np.inf, col] = val

    return df


from sklearn.cluster import KMeans
from collections import Counter




    # sys.exit(0)

def distance_to_centroid(features,row, centroid):
    # print features
    row2 = row[features]
    ec = euclidean(row2, centroid)

    return ec

def dokmeans(df, features_to_use):



    for k in [5, 10 ,15, 20, 25, 30]:
        print k
        kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[['latitude','longitude']])
        df['skmeans'+str(k)] = kmeans.predict(df[['latitude','longitude']])
        for i in range(k):
            # print kmeans.cluster_centers_[i]
            df['lldistance_to_center'+str(k)+'_'+str(i)] = df.apply(lambda r: distance_to_centroid(['latitude','longitude'],r, kmeans.cluster_centers_[i]), axis=1)
            features_to_use +=['lldistance_to_center'+str(k)+'_'+str(i)]
            print df['lldistance_to_center'+str(k)+'_'+str(i)]

        # listing_weeks = df['listing_id'].groupby(df['skmeans'+str(k)]).transform('count')
        # price_weeks = df['price'].groupby(df['skmeans'+str(k)]).transform('sum')
        # avg_weeks = price_weeks/listing_weeks
        #
        # df['skmeans'+str(k)+'avg_price'] = df['skmeans'+str(k)].map(lambda x: avg_weeks[x])
        # df['skmeans'+str(k)+'price_week'] = df['price']/df['skmeans'+str(k)+'avg_price']
        # features_to_use.append('skmeans'+str(k)+'avg_price')
        # features_to_use.append('skmeans'+str(k)+'price_week')


    for k in [5, 10 ,15, 20, 25, 30]:
        print k
        kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[['latitude','longitude', 'price', 'bathrooms', 'bedrooms']])
        df['kmeans'+str(k)] = kmeans.predict(df[['latitude','longitude','price', 'bathrooms', 'bedrooms']])
        for i in range(k):
            print kmeans.cluster_centers_[i]
            df['distance_to_center'+str(k)+'_'+str(i)] = df.apply(lambda r: distance_to_centroid(['latitude','longitude', 'price', 'bathrooms', 'bedrooms'],r, kmeans.cluster_centers_[i]), axis=1)
            features_to_use +=['distance_to_center'+str(k)+'_'+str(i)]
        # preds =  kmeans.predict(df.iloc[num_train:][['latitude','longitude','price', 'bathrooms', 'bedrooms']])

        # df['kmeans'+str(k)+'avg_price'] = df['skmeans'+str(k)].map(lambda x: avg_weeks[x])
        # df['kmeans'+str(k)+'price_week'] = df['price']/df['kmeans'+str(k)+'avg_price']
        # features_to_use.append('kmeans'+str(k)+'avg_price')
        # features_to_use.append('kmeans'+str(k)+'price_week')

    for k in [5, 10 ,15, 20, 25, 30]:
        print k
        kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[['word_desc', 'word_features', 'prize_feature', 'prize_desc','length_desc','length_features', 'pictures_length']])
        df['wkmeans'+str(k)] = kmeans.predict(df[['word_desc', 'word_features', 'prize_feature', 'prize_desc','length_desc','length_features', 'pictures_length']])
        for i in range(k):
            print kmeans.cluster_centers_[i]
            df['wdistance_to_center'+str(k)+'_'+str(i)] = df.apply(lambda r: distance_to_centroid(['word_desc', 'word_features', 'prize_feature', 'prize_desc','length_desc','length_features', 'pictures_length'],r, kmeans.cluster_centers_[i]), axis=1)
            features_to_use +=['wdistance_to_center'+str(k)+'_'+str(i)]


    # for k in [5, 10 ,15, 20, 25]:
    #     print k
    #     kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[['year', 'month', 'day', 'weekday', 'hour']])
    #     df['tkmeans'+str(k)] = kmeans.predict(df[['year', 'month', 'day', 'weekday', 'hour']])

    with_this= ['latitude','longitude','price', 'bathrooms', 'bedrooms', 'price_to_max_low', 'price_to_max_high', 'price_to_max_medium','length_desc','length_features', 'manager_id_enc', 'building_id_enc']
    # 'lda10','lda20','lda30','lda40','lda50','lda80','lda100']
    for k in [5, 10 ,15, 20, 25, 30]:
        print k
        kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[with_this])
        df['bkmeans'+str(k)] = kmeans.predict(df[with_this])
        for i in range(k):
            print kmeans.cluster_centers_[i]
            df['bdistance_to_center'+str(k)+'_'+str(i)] = df.apply(lambda r: distance_to_centroid(with_this,r, kmeans.cluster_centers_[i]), axis=1)
            features_to_use +=['bdistance_to_center'+str(k)+'_'+str(i)]



    return df



# df_with_distances = calc_distances(df_all_new_features)
#
# sys.exit(0)
from bs4 import BeautifulSoup
def remove_html(x):
    cleantext = BeautifulSoup(x).text
    return cleantext

# df_few = pd.read_csv(open('data/train_few_features.csv', 'r'), encoding='utf-8')
# df_dist = pd.read_csv(open('data/train_distances.csv', 'r'), encoding='utf-8')
# df_lda = pd.read_csv(open('data/train_description_groups_with_data.csv', 'r'), encoding='utf-8')
# df_kmeans = pd.read_csv(open('data/train_description_kmeans2_groups_with_data.csv', 'r'), encoding='utf-8')
# df_kmeans2 = pd.read_csv(open('data/train_description_kmeans_groups_with_data.csv', 'r'), encoding='utf-8')
# df_kmeans3 = pd.read_csv(open('data/train_description_kmeans3_groups_with_data.csv', 'r'), encoding='utf-8')
# df_kmeans5 = pd.read_csv(open('data/train_description_kmeans5_groups_with_data.csv', 'r'), encoding='utf-8')
# df_image_size = pd.read_csv(open('data/train_image_size.csv', 'r'), encoding='utf-8')
# df_image_color = pd.read_csv(open('data/train_image_colors.csv', 'r'), encoding='utf-8')
# df_mean_enc = pd.read_csv(open('data/meancoded_manager_id.csv', 'r'), encoding='utf-8')
# df_mean_acc = pd.read_csv(open('data/train_acc_price_mgr.csv', 'r'), encoding='utf-8')
# df_mean_acc_b = pd.read_csv(open('data/train_acc_price_building.csv', 'r'), encoding='utf-8')
# df_mean_acc_p = pd.read_csv(open('data/train_acc_price_price.csv', 'r'), encoding='utf-8')
# df_mean_acc_i = pd.read_csv(open('data/train_acc_bed_price_i.csv', 'r'), encoding='utf-8')
# df_mean_acc_bed = pd.read_csv(open('data/train_acc_bed_price.csv', 'r'), encoding='utf-8')
df_mean_acc_bed = pd.read_csv(open('data/train_many_features.csv', 'r'), encoding='utf-8')

features_to_use = df_mean_acc_bed.columns.values
# df_mean_acc_bed = pd.read_csv(open('data/train_acc_bed_price1.csv', 'r'), encoding='utf-8')

features_to_use = features_to_use.tolist()
features_to_use.remove('listing_id')
# print features_to_use

# df_all_new = pd.merge(df_all, df_few, how='left', on='listing_id')
df_all_new = pd.merge(df_all, df_mean_acc_bed, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_lda, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_kmeans, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_kmeans2, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_kmeans3, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_kmeans5, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_image_size, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_image_color, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_mean_enc, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_mean_acc, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_mean_acc_b, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_mean_acc_p, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_mean_acc_i, how='left', on='listing_id')
# df_all_new = pd.merge(df_all_new, df_mean_acc_bed, how='left', on='listing_id')





import webcolors

def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.css3_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

# for col in ['color1', 'color2', 'color3']:
#     # df_all_new.loc[df_all_new[col]==float('Na'), col] = '#0'
#     df_all_new[col] = df_all_new[col].map(lambda x: zeroIfnanS(x,'#000000'))
#     print df_all_new[col]
#     df_all_new[col] = df_all_new[col].map(lambda x: closest_colour(webcolors.hex_to_rgb(x)))
#
#     encc = LabelEncoder()
#     encc.fit(df_all_new[col])
#     df_all_new[col+"_enc"] = encc.transform(df_all_new[col])

# df_all_new['color_names'] = df_all_new['color1'] + " " + df_all_new['color2'] + " " + df_all_new['color3']
# print df_all_new['color_names']
#     # features_to_use.append(col)
#
# print features_to_use

# df_all_new=doKneighbours(df_all_new)

def address_1(x):
    if not isinstance(x, basestring):
        return ''
    x = x.lower()
    x = x.replace(' e ', ' east ')
    x = x.replace(' w ', ' west ')
    x = x.replace(' s ', ' south ')
    x = x.replace(' n ', ' north ')
    x = re.sub(r'^e ', 'east ', x)
    x = re.sub(r'^w ', 'west ', x)
    x = re.sub(r'^s ', 'south ', x)
    x = re.sub(r'^n ', 'north ', x)
    x = x.replace(' st.', ' street')
    x = x.replace(' st ', ' street')
    x = x.replace(' ave.', ' avenue')
    x = x.replace(' pl.', ' place')
    x = x.replace(' pl', ' place')

    x = re.sub(r' sq$', ' square', x)
    x = re.sub(r' st$', ' street', x)
    x = re.sub(r' ave$', ' avenue', x)

    x = x.replace('first', '1st')
    x = x.replace('second', '2nd')
    x = x.replace('third', '3rd')
    x = x.replace('fourth', '4th')
    x = x.replace('fifth', '5th')
    x = x.replace('sixth', '5th')
    x = x.replace('seventh', '7th')
    x = x.replace('eighth', '8th')
    x = x.replace('ninth', '9th')
    x = x.replace('tenth', '10th')
    x = x.replace('eleventh', '11th')
    x = x.replace('twelveth', '12th')

    x = x.replace('.', '')
    x = x.replace(',', ' ')
    x = x.strip()

    return x

df_all_new['display_address_new'] = df_all_new['display_address'].map(lambda x: address_1(x))
df_all_new['street_address_new'] = df_all_new['street_address'].map(lambda x: address_1(x))
# df_all_new['street_address_new'] = df_all_new['street_address'].map(lambda x: address_1(x))
# df_all_new['new_features_desc'] = df_all_new['features'] + " - " +df_all_new['description']
#
df_all_new['new_features'] = df_all_new['features'].map(lambda x: change_features(x))
# df_all_new['new_features_desc'] = df_all_new['new_features_desc'].map(lambda x: remove_punctuation(x))
df_all_new['new_features'] = df_all_new['new_features'].map(lambda x: remove_punctuation(x))

def isEast(x):
    if 'east' in x:
        return 1
    elif 'west' in x:
        return 0
    else:
        return np.nan

df_all_new['east'] = df_all_new['display_address_new'].map(lambda x: isEast(x))

# print df_all_new[['display_address_new','street_address']]

# df_all_new['display_address_new_2'] = df_all_new['display_address_new'].map(lambda x: " ".join(x.split(' ')[-2:]))
# print df_all_new['display_address_new_2']
enc = LabelEncoder()
# df_all_new['address_enc_2'] = enc.fit_transform(df_all_new['display_address_new'])

# df_all_new['street_address_enc_2'] = enc.fit_transform(df_all_new['street_address_new'])


# print np.unique(df_all_new['address_enc_2'])

# sys.exit(0)
# df_all_new = first_updates(df_all_new)
# df_all_new = new_round(df_all_new)
# df_all_new=dokmeans(df_all_new, features_to_use)
#
# features_to_store = features_to_use+['listing_id']
# df_all_new[features_to_store].to_csv('data/train_many_features.csv', index=False)


from numpy import inf
# df_all_new['price_be'][df_all_new['price_be'] == inf] = 0
# df_all_new['price_ba'][df_all_new['price_ba'] == inf] = 0

# print df_all_new['medium_mgr_list_avg']

df_all_new['price'] = df_all_new['price_x']
df_all_new['bathrooms'] = df_all_new['bathrooms_x']
df_all_new['bedrooms'] = df_all_new['bedrooms_x']
df_all_new['latitude'] = df_all_new['latitude_x']
df_all_new['longitude'] = df_all_new['longitude_x']





import os
import sys
import operator
import numpy as np
import pandas as pd
from scipy import sparse
import xgboost as xgb
from sklearn import model_selection, preprocessing, ensemble
from sklearn.metrics import log_loss
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import math

def runXGB(train_X, train_y, test_X, test_y=None, feature_names=None, seed_val=0, num_rounds=1000):
    param = {}
    param['nthread'] = 3
    param['objective'] = 'multi:softprob'
    param['eta'] = 0.05
    param['gamma'] = 0.1
    param['max_depth'] = 5
    param['silent'] = 1
    param['num_class'] = 3
    param['eval_metric'] = "mlogloss"
    param['min_child_weight'] = 10
    param['subsample'] = 0.8
    param['colsample_bytree'] = 0.9
    param['colsample_bylevel'] = 0.75
    param['seed'] = seed_val
    param['base_score']= 0.75
    num_rounds = num_rounds

    plst = list(param.items())
    xgtrain = xgb.DMatrix(train_X, label=train_y)

    if test_y is not None:
        xgtest = xgb.DMatrix(test_X, label=test_y)
        watchlist = [ (xgtrain,'train'), (xgtest, 'test') ]
        model = xgb.train(plst, xgtrain, num_rounds, watchlist, early_stopping_rounds=20)
    else:
        xgtest = xgb.DMatrix(test_X)
        model = xgb.train(plst, xgtrain, num_rounds)

    pred_test_y = model.predict(xgtest)
    return pred_test_y, model

df_train = df_all_new.iloc[:num_train]
df_test = df_all_new.iloc[num_train:]


print df_train.shape
df_train = df_train[df_train['price'] > 1000]
print df_train.shape

train_y = df_train['interest_level']
train_X = df_train[:]
test_X = df_test[:]

train_df= df_train
test_df = df_test
print(train_df.shape)
print(test_df.shape)

tfidf = CountVectorizer(ngram_range=(1, 2))
tr_sparse = tfidf.fit_transform(train_df["new_features"])
te_sparse = tfidf.transform(test_df["new_features"])

train_X = sparse.hstack([train_df[features_to_use], tr_sparse]).tocsr()
test_X = sparse.hstack([test_df[features_to_use], te_sparse]).tocsr()

target_num_map = {'high':0, 'medium':1, 'low':2}
train_y = np.array(train_df['interest_level'].apply(lambda x: target_num_map[x]))
print(train_X.shape, test_X.shape)

cv_scores = []
from sklearn.model_selection import StratifiedKFold
kf = StratifiedKFold(n_splits=4, shuffle=True, random_state=2016)
# for dev_index, val_index in kf.split(range(train_X.shape[0])):
for dev_index, val_index in kf.split(train_X, train_y):
        dev_X, val_X = train_X[dev_index,:], train_X[val_index,:]
        dev_y, val_y = train_y[dev_index], train_y[val_index]
        preds, model = runXGB(dev_X, dev_y, val_X, val_y)
        cv_scores.append(log_loss(val_y, preds))
        print(cv_scores)
        # break

preds, model = runXGB(train_X, train_y, test_X, num_rounds=850)
out_df = pd.DataFrame(preds)
out_df.columns = ["high", "medium", "low"]
out_df["listing_id"] = test_df.listing_id.values
out_df.to_csv("submission_new_xgb.csv", index=False)
