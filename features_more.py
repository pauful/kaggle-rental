import numpy as np
import pandas as pd
import sys
from Levenshtein import distance
import re
from collections import defaultdict
from nltk.stem.porter import *
stemmer = PorterStemmer()
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

pd_train = pd.read_json('data/train-formatted.json')
pd_test = pd.read_json('data/test-formatted.json')

pd_all = pd.concat([pd_train, pd_test])

from bs4 import BeautifulSoup
def remove_html(x):
    cleantext = BeautifulSoup(x).text
    return cleantext

# from collections import Counter
# print Counter(pd_train[pd_train['description'].str.contains('\?')]['interest_level'])
# sys.exit(0)
from nltk.corpus import stopwords
def change_features(x):


    x = x.lower()

    x = remove_html(x)

    x = x.strip(' ')
    x = x.strip('\t')
    x = x.strip('\n')
    x = x.strip('\r')
    x = re.sub(r'\d+', ' ', x)

    x = x.replace('/', ' ')
    x = x.replace('!', ' ')
    x = x.replace('(', ' ')
    x = x.replace(')', ' ')
    x = x.replace('...', ' ')
    x = x.replace('..', ' ')
    x = x.replace('"', ' ')
    x = x.replace('\'', '')
    x = x.replace('___', ' ')
    x = x.replace('!', ' ')
    x = x.replace('?', ' ')
    x = x.replace('&', ' and ')
    x = x.replace('\r', ' ')
    x = x.replace('\n', ' ')
    x = x.replace('\r\r', ' ')
    x = x.replace('<U+0082>', ' ')
    x = x.replace('<U+0083>', ' ')
    x = x.replace('<U+009D>', ' ')
    x = x.replace('<U+2028>', ' ')

    x = x.replace('xlarge', 'extra large')


    x = x.replace('pre war', 'prewar')
    x = x.replace('pre-war', 'prewar')

    x = x.replace('post war', 'postwar')
    x = x.replace('post-war', 'postwar')

    x = x.replace('dish washer', 'dishwasher')
    x = x.replace('fire place', 'fireplace')
    x = x.replace('walk in', 'walkin')
    x = x.replace('walk-in', 'walkin')
    x = x.replace('on site', 'onsite')
    x = x.replace('on-site', 'onsite')
    x = x.replace('counter-tops', 'countertops')
    x = x.replace('counter tops', 'countertops')
    x = x.replace('roof-deck', 'roofdeck')
    x = x.replace('roof deck', 'roofdeck')
    x = x.replace('eat in', 'eatin')
    x = x.replace('eat-in', 'eatin')
    x = x.replace('walk up', 'walkup')
    x = x.replace('over-size', 'oversize')
    x = x.replace('over size', 'oversize')
    x = x.replace('low-rise', 'lowrise')
    x = x.replace('low rise', 'lowrise')
    x = x.replace('doormen', 'doorman')
    x = x.replace('door man', 'doorman')
    x = x.replace('door men', 'doorman')
    x = x.replace('wat ', 'water ')
    x = x.replace('wash dryer', 'washer dryer')
    x = x.replace('washer & dryer', 'washer dryer')
    x = x.replace('washer and dryer', 'washer dryer')
    x = x.replace('wi fi', 'wifi')
    x = x.replace('wi-fi', 'wifi')
    x = x.replace('laundri', 'laundry')
    x = x.replace('laundri', 'laundry')

    x = x.replace('pets ok', 'pets allowed')
    x = x.replace('a c', 'air conditioning')

    x = x.replace('high ceilings', 'high ceiling')
    x = x.replace('outdoor areas', 'outdoor space')

    x = x.replace('wifi access', 'wifi')
    x = x.replace('washer in unit', 'washer')




    stop = stopwords.words('english')
    x = (" ").join([z for z in x.split(" ") if z not in stop])

    x = re.sub(r';|,|\-|\*|\~|\.|\||:', ' ', x)
    x = x.strip()

    x = x.replace('www', ' ')
    x = x.replace(' com ', ' ')
    x= re.sub(' +',' ',x)
    x = (" ").join([stemmer.stem(z) for z in x.split(" ")])
    x = x.encode("utf-8")

    return x


#######################################

# Create dataset



# pd_all['description'] =  pd_all['description'].map(lambda x: change_features(x))
# pd_all['description'] = pd_all['description'].replace(np.nan,'', regex=True)
# pd_all['description'].fillna('', inplace=True)
# pd_all['description'] = pd_all['description'].astype(str)
#
# print  pd_all['description']
#
# pd_all[['listing_id','description']].to_csv('data/train_description_groups.csv', encoding='utf-8', index=False, quotechar="\"")
#
# print 'done'
##########################################

from collections import Counter
# pd_all.loc[(pd_all['bedrooms'] == 0) & pd_all['description'].str.contains('bed') ,'bedrooms']=1
aaa = pd_all[(pd_all['bedrooms'] == 0) & pd_all['description'].str.contains('bed') ]['description']
for aa in aaa:
    print aa

# aaa = pdpd[pdpd['description'].str.contains('bed')]['interest_level']

# pd_all['bedrooms'] = pd_all[['bedrooms', 'description']].map(lambda x: x[0])

print Counter(aaa)
aaa = pd_all[(pd_all['bedrooms'] == 0)  ]['interest_level']
print Counter(aaa)
sys.exit(0)

pd_all = pd.read_csv(open('data/train_description_groups.csv', 'r'), encoding='utf-8')
pd_all['description'].fillna('', inplace=True)
pd_all['description'] = pd_all['description'].map(lambda x: x.encode('utf-8'))
pd_all['description'] = pd_all['description'].astype(str)

# pd_all_data = pd.read_csv(open('data/train_description_groups_with_data.csv', 'r'), encoding='utf-8')
# pd_all = pd.merge(pd_all, pd_all_data, how='left', on='listing_id')

from gensim import corpora, models, similarities



nn = []
for a in pd_all['description'].tolist():
    k = [bb for bb in a.split(' ') if bb != '' and  not isinstance(bb, float)]
    nn.append(k)

# stop = stopwords.words('english')
# nn = []
# for intermediate in ccc:
#     intermediate2 = [i for i in intermediate if i not in stop]
#     nn.append(intermediate2)

dictionary = corpora.Dictionary(nn)
corpus = [dictionary.doc2bow(text) for text in nn]
tfidf = models.TfidfModel(corpus)
tcorpus = tfidf[corpus]
print 'calculus'
np.random.seed(42)
# model = models.lsimodel.LsiModel(corpus=tcorpus, id2word=dictionary, num_topics=2)
def findTopic(x, tfidf, dictionary, model):
    k = [bb for bb in x.split(' ') if bb != '' and  not isinstance(bb, float)]

    classes = model[tfidf[dictionary.doc2bow(k)]]

    id_max = 0
    per_max = -100
    for idx, per in enumerate(classes):
        if per > per_max:
            id_max = idx
            per_max = per

    return id_max

def model(df, topics, tcorpus, dictionary, tfidf):
    model = model = models.HdpModel(corpus=tcorpus, id2word=dictionary)
    # model  = models.RpModel(corpus=tcorpus, id2word=dictionary, num_topics=topics)
    # model = models.lsimodel.LsiModel(corpus=tcorpus, id2word=dictionary, num_topics=topics)

    # for t in  model.show_topics(num_topics=-1, num_words=10, log=False, formatted=True):
    #     print t

    index = similarities.MatrixSimilarity(model[tcorpus])

    df['lda'+str(topics)] = df['description'].map(lambda x: findTopic(x, tfidf, dictionary, model))

    from collections import Counter
    print topics
    print Counter(df['lda'+str(topics)])

    return df

for t in [10]:
    pd_all = model(pd_all, t, tcorpus, dictionary, tfidf)

# pd_all[['listing_id', 'lda10', 'lda20', 'lda30', 'lda40', 'lda50', 'lda80', 'lda100']].to_csv('data/train_description_groups_with_data.csv', encoding='utf-8', index=False, quotechar="\"")

# import ipdb; ipdb.set_trace()
sys.exit(0)
















#
# ###########################

features = pd_all['features_new'].tolist()
features = ",".join(features)
l = re.split(r';|,|\-|\*|\~|\.|and', features)
# l = ",".join(features).lower().split(",")
l = [kk.strip(' ') for kk in l]
l = [kk.strip('\t') for kk in l]
l = [kk.strip('\n') for kk in l]

# l = set(l)


import collections
counter=collections.Counter(l)
for i in counter.most_common(100):
    print i[0] + ' - ' + str(i[1])

print
print

aa= [i[0] for i in counter.most_common(100)]
for i in sorted(aa):
    print i



sys.exit(0)

print( len([key for key, value in ll.iteritems()]))

repeated = [key.strip() for key, value in ll.iteritems() if value > 1]

notrepeated = [key.strip() for key, value in ll.iteritems() if value == 1]

lrepeated = list(set(repeated))
for i in sorted(repeated):
   print i + " " + str(ll[i])
print(len(repeated))
print(len(notrepeated))
# print(len(notrepeated))

# for i in repeated:
#     for k in repeated:
#         if i != k:
#             dist = distance(i, k)
#             if dist < 3:
#                 print(i + " ### " +k)

# found = True
# for i in range(10):
#     print i
#     if found:
#         found = False
#         newarr = repeated
#         for x in repeated:
#             for y in newarr:
#                 w = repeated
#                 if x != y and x in y and x != '' and len(x) > 2:
#                     # print(rep)
#                     # print(nrep)
#                     w = y.replace(x, "")
#                     w = w.strip()
#                     found = True
#                     newarr.append(w)
#                 elif y != '' and x != y and x != y:
#                     newarr.append(w)
#
#             newarr = list(set(newarr))
#         repeated = list(set(newarr))
#         print(len(repeated))
#
# print(repeated)
# print(len(repeated))


# aa =  [v.split(" ") for v in aa]

# flatten = lambda l: [item for sublist in aa for item in sublist]
# print flatten(aa)


# aa = flatten(aa)
# import enchant
# d = enchant.Dict("en_US")
# aa = set(aa)
# aasugg = [{'word':w,'sug':d.suggest(w)} for w in aa if w != '' and len(w) > 1 and not( d.check(w) or d.check(w.upper()) or  d.check(w[0].upper()+w[1:]))]
#
# import pprint
# #
# pp = pprint.PrettyPrinter(indent=4)
# pp.pprint(ll)
#
# print len(ll)
# print len(aa)
# print len(aasugg)
# #print len(set(l))
#
