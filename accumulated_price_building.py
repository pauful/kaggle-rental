import datetime
import pandas as pd
train_df = pd.read_json('data/train-formatted.json')
test_df = pd.read_json('data/test-formatted.json')
df_build = pd.read_csv(open('data/train_new_building.csv', 'r'), encoding='utf-8')
df = pd.concat([train_df, test_df], 0)

df = pd.merge(df, df_build, how='left', on='listing_id')

df["created"] = pd.to_datetime(df["created"])

mintime = df['created'].min()



def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0

mintime = unix_time_millis(mintime)

df["millis"] = df["created"].map(lambda x: unix_time_millis(x))



df['millis_str'] = df['millis'].apply(str)
df['mgr_list'] = df['building_id2'] + '#'+ df['millis_str']

df['building_list_count']=df.apply(lambda x: df[(df['building_id2'] == x['building_id2']) & (df['millis'] <= x['millis'])]['price'].count(), axis=1)
df['building_list_sum']=df.apply(lambda x: df[(df['building_id2'] == x['building_id2']) & (df['millis'] <= x['millis'])]['price'].sum(), axis=1)
# df['building_list_count']= df.groupby('building_id2')[df['millis'] <= x['millis']]['price'].count()
# df['building_list_sum']= df.groupby('building_id2')[df['millis'] <= x['millis']]['price'].sum()
df['building_list_avg'] = df['building_list_sum'] /df['building_list_count']

df[['listing_id','building_list_count','building_list_sum', 'building_list_avg']].to_csv('data/train_acc_price_building2.csv', encoding='utf-8', index=False)
