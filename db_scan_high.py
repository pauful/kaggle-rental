#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :
import gpxpy.geo
import time, datetime, json
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics import  make_scorer
from  sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import StratifiedKFold
from nltk.stem.porter import *
from scipy.spatial.distance import euclidean
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata
from sklearn.feature_selection import SelectKBest, f_classif, f_regression
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import xgboost as xgb
import sklearn.utils
from sklearn.utils import check_array, check_consistent_length
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN
from sklearn.neighbors import KDTree

pd_train = pd.read_json('data/train-formatted.json')
pd_test = pd.read_json('data/test-formatted.json')
id_test = pd_test['listing_id']



num_train = pd_train.shape[0]
df_all = pd.concat((pd_train, pd_test), axis=0, ignore_index=True)

from collections import Counter

df_all_medium = pd_train[pd_train['interest_level'] == 'medium']
# df_all_not_medium = pd_train[pd_train['interest_level'] == '']
coordinates= df_all_medium[['latitude','longitude']].sample(frac=0.7)
coordinates2= df_all[['latitude','longitude']]
kms_per_radian = 6371.0088

# model = DBSCAN(eps=(0.02)/kms_per_radian, min_samples=10, algorithm='ball_tree', metric='haversine')
# print model.fit_predict(np.radians(coordinates))
print KDTree.valid_metrics
tree = KDTree(coordinates, leaf_size=2, metric='cityblock')
print tree.query(coordinates2, k=10)[0]
print
print tree.query(coordinates, k=10)[0]
# sys.exit(0)
def avd_res(x):
    res = tree.query(x[['latitude','longitude']].reshape((1, -1)), k=10)[0]
    avg = 0.0
    for i in res[0]:
        avg += i

    return avg /10

df_all['avg_dts_to_medium'] = df_all.apply(lambda x: avd_res(x) , axis=1)

df_all[['listing_id', 'avg_dts_to_medium']].to_csv('data/kdtree_medium.csv')

# for i in range(5):
#     df_all['dbscan_'+str(i)] = DBSCAN(eps=(0.02*(i+1))/kms_per_radian, min_samples=30, algorithm='ball_tree', metric='haversine').fit_predict(np.radians(coordinates))
#     # df_all['dbscan_'+str(i)] = db.predict(coordinates)
#     print df_all['dbscan_'+str(i)]
#     print Counter(df_all['dbscan_'+str(i)])
#
# df_all[['listing_id','dbscan_0','dbscan_1','dbscan_2','dbscan_3','dbscan_4']].to_csv('data/train_dbscan.csv', index=False)
