import datetime
import pandas as pd
train_df = pd.read_json('data/train-formatted.json')
test_df = pd.read_json('data/test-formatted.json')

df = pd.concat([train_df, test_df], 0)

df["created"] = pd.to_datetime(df["created"])

mintime = df['created'].min()



def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0

mintime = unix_time_millis(mintime)

df["millis"] = df["created"].map(lambda x: unix_time_millis(x))

agg_prices_time = (df.
      sort_values("millis").
      groupby(['building_id',"millis", 'listing_id']).
      agg(sum)['price'].
      reset_index("millis"))

df['millis_str'] = df['millis'].apply(str)
df['mgr_list'] = df['building_id'] + '#'+ df['millis_str']

# df['building_list_count']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == x.split('#')[0]) & (agg_prices_time['millis'] <= float(x.split('#')[1]))]['price'].count())
# df['building_list_sum']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == x.split('#')[0]) & (agg_prices_time['millis'] <= float(x.split('#')[1]))]['price'].sum())
df['building_list_count_b'] = df.apply(lambda x: df[(df['building_id'] == x['building_id']) & (df['millis'] <= x['millis']]['building_id'].count())

df[['listing_id','building_list_count_b']].to_csv('data/train_building_count_before.csv', encoding='utf-8', index=False)
