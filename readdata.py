import json, sys
from collections import defaultdict

with open(sys.argv[1], 'r') as json_data:
    print sys.argv[1]
    d = json.load(json_data)
    data = defaultdict(dict)
    for key in d.keys():
        for entry in d[key].keys():
            if key not in  data[entry]:
                data[entry][key] = data[entry][key] = d[key][entry]

    dcsv = []
    for key in data.keys():
        ob = {}
        if 'listing_id' not in data[key]:
            print data[key]
            sys.exit(1)
        ob['listing_id'] = data[key]['listing_id']
        if 'interest_level' not in data[key] and "test" not in sys.argv[1]:
            print data[key]
            sys.exit(1)
        if 'interest_level' in data[key]: 
            ob['interest_level'] = data[key]['interest_level']

        if 'display_address' in data[key]:
            ob['display_address'] = data[key]['display_address']

	
        if 'description' in data[key]:
            ob['description'] = data[key]['description']

        if 'created' in data[key]:
            ob['created'] = data[key]['created']

	if 'price' in data[key]:
            ob['price'] = data[key]['price']

	if 'bedrooms' in data[key]:
            ob['bedrooms'] = data[key]['bedrooms']

	if 'longitude' in data[key]:
            ob['longitude'] = data[key]['longitude']

	if 'photos' in data[key]:
            ob['photos'] = ",".join(data[key]['photos'])

	if 'latitude' in data[key]:
            ob['latitude'] = data[key]['latitude']

	if 'bathrooms' in data[key]:
            ob['bathrooms'] = data[key]['bathrooms']

	if 'building_id' in data[key]:
            ob['building_id'] = data[key]['building_id']

	if 'street_address' in data[key]:
            ob['street_address'] = data[key]['street_address']

	if 'features' in data[key]:
            ob['features'] = ",".join(data[key]['features'])

	if 'manager_id' in data[key]:
            ob['manager_id'] = data[key]['manager_id']



 	dcsv.append(ob)


    import re
    filename = re.search(".*\/(.*)\.json", sys.argv[1]).group(1)
    with open('data/{}-formatted.json'.format(filename), 'w') as of:
        json.dump(dcsv, of)



