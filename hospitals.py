import pandas as pd
import sys
from Levenshtein import distance
import re
from collections import defaultdict
from nltk.stem.porter import *
stemmer = PorterStemmer()
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

pd_train = pd.read_csv('data/hospitals.csv')

pd_train['place'] = pd_train['Location 1'].map(lambda x: x.split('(')[1].split(')')[0])

print pd_train['place']
