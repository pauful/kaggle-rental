import datetime
import pandas as pd
train_df = pd.read_json('data/train-formatted.json')
test_df = pd.read_json('data/test-formatted.json')
num_train = train_df.shape[0]
df = pd.concat([train_df, test_df], 0)

df["created"] = pd.to_datetime(df["created"])

mintime = df['created'].min()



def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0

mintime = unix_time_millis(mintime)

df["millis"] = df["created"].map(lambda x: unix_time_millis(x))

df_train = df.iloc[:num_train]

agg_prices_time = (df_train.
      sort_values("millis").
      groupby(['interest_level',"millis"]).
      agg(sum)['price'].
      reset_index("millis"))


print agg_prices_time
df['millis_str'] = df['millis'].apply(str)
df['interest_level'] = df['interest_level'].fillna('')
df['mgr_list'] = df['interest_level'] + '#'+ df['millis_str']

df['high_mgr_list_sum']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == 'high') & (agg_prices_time['millis'] <= float(x.split('#')[1]))]['price'].sum())
df['high_mgr_list_count']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == 'high') & (agg_prices_time['millis'] <= float(x.split('#')[1]))]['price'].count())
df['medium_list_sum']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == 'medium') & (agg_prices_time['millis'] <= float(x.split('#')[1]))]['price'].sum())
df['medium_list_count']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == 'medium') & (agg_prices_time['millis'] <= float(x.split('#')[1]))]['price'].count())

df['high_mgr_list_avg'] = df['high_mgr_list_sum'] /df['high_mgr_list_count']
df['medium_mgr_list_avg'] = df['medium_list_sum'] /df['medium_list_count']

df[['listing_id','high_mgr_list_avg','medium_mgr_list_avg']].to_csv('data/train_acc_price_price.csv', encoding='utf-8', index=False)
