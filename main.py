#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :
import gpxpy.geo
import time, datetime, json
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics import  make_scorer
from  sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import StratifiedKFold
from nltk.stem.porter import *
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata
from sklearn.feature_selection import SelectKBest, f_classif, f_regression
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import xgboost as xgb
import sklearn.utils
from sklearn.utils import check_array, check_consistent_length
from sklearn.decomposition import PCA

import sys
reload(sys)
sys.setdefaultencoding('utf8')
def _weighted_sum(sample_score, sample_weight, normalize=False):
    if normalize:
        return np.average(sample_score, weights=sample_weight)
    elif sample_weight is not None:
        return np.dot(sample_score, sample_weight)
    else:
        return sample_score.sum()

def log_loss(y_true, y_pred, eps=1e-15, normalize=True, sample_weight=None,
             labels=None):

    y_pred = check_array(y_pred, ensure_2d=False)
    check_consistent_length(y_pred, y_true)

    lb = LabelBinarizer()

    if labels is not None:
        lb.fit(labels)
    else:
        lb.fit(y_true)

    if len(lb.classes_) == 1:
        if labels is None:
            raise ValueError('y_true contains only one label ({0}). Please '
                             'provide the true labels explicitly through the '
                             'labels argument.'.format(lb.classes_[0]))
        else:
            raise ValueError('The labels array needs to contain at least two '
                             'labels for log_loss, '
                             'got {0}.'.format(lb.classes_))

    transformed_labels = lb.transform(y_true)

    if transformed_labels.shape[1] == 1:
        transformed_labels = np.append(1 - transformed_labels,
                                       transformed_labels, axis=1)

    # Clipping
    y_pred = np.clip(y_pred, eps, 1 - eps)

    # If y_pred is of single dimension, assume y_true to be binary
    # and then check.
    if y_pred.ndim == 1:
        y_pred = y_pred[:, np.newaxis]
    # print( y_pred.shape)
    if y_pred.shape[1] == 1:
        y_pred = np.append(1 - y_pred, y_pred, axis=1)

    # Check if dimensions are consistent.
    transformed_labels = check_array(transformed_labels)
    if len(lb.classes_) != y_pred.shape[1]:
        if labels is None:
            raise ValueError("y_true and y_pred contain different number of "
                             "classes {0}, {1}. Please provide the true "
                             "labels explicitly through the labels argument. "
                             "Classes found in "
                             "y_true: {2}".format(transformed_labels.shape[1],
                                                  y_pred.shape[1],
                                                  lb.classes_))
        else:
            raise ValueError('The number of classes in labels is different '
                             'from that in y_pred. Classes found in '
                             'labels: {0}'.format(lb.classes_))

    # Renormalize

    y_pred /= y_pred.sum(axis=1)[:, np.newaxis]

    loss = -(transformed_labels * np.log(y_pred)).sum(axis=1)



    return _weighted_sum(loss, sample_weight, normalize)


features_to_use = ['bathrooms', 'bedrooms', 'price', 'length_features', 'length_desc',
'pictures_length', 'year', 'month', 'day', 'weekday', 'price_be', 'price_ba',  'num_features', 'word_desc', 'word_features', 'prize_feature', 'prize_desc',
'price_be_ba', 'manager_id_enc', 'building_id_enc', 'price_avg_manager', 'price_avg_building', 'manager_id_count', 'building_id_count',
'pictures_length_avg_building', 'pictures_length_avg_manager',
'avg_pic_room',
# 'bed_&_bath','bed_or_bath',
'avg_length_desc', 'avg_length_features',
'latitude', 'longitude',
# 'listing_months','listing_days', 'listing_weeks',
'price_to_max_low', 'price_to_max_high', 'price_to_max_medium',
'hour','am','pm',
'week','kmeans5','kmeans10','kmeans25','kmeans15','kmeans20','kmeans30',
'bkmeans5','bkmeans10','bkmeans25','bkmeans15','bkmeans20','bkmeans30',
'skmeans5','skmeans10','skmeans25','skmeans15','skmeans20','skmeans30',
'wkmeans5','wkmeans10','wkmeans25','wkmeans15','wkmeans20','wkmeans30',
# 'varkmeans5','varkmeans10','varkmeans15','varkmeans20','varkmeans25','varkmeans30',
#'pkmeans5','pkmeans10','pkmeans25','pkmeans15','pkmeans20','pkmeans30',
# 'tkmeans5','tkmeans10','tkmeans25','tkmeans15','tkmeans20',
# 'neighbours', 'bneighbours',
 'exclamation', 'interrognat', #'interrognat_exc',
 'lda10','lda20','lda30','lda40','lda50','lda80','lda100',
 'des_kmeans2_5','des_kmeans2_10','des_kmeans2_15','des_kmeans2_20','des_kmeans2_25',
 'des_kmeans3_5','des_kmeans3_10','des_kmeans3_15','des_kmeans3_20','des_kmeans3_25',
 # 'des_kmeans5_5','des_kmeans5_10','des_kmeans5_15','des_kmeans5_20','des_kmeans5_25',
 'des_kmeans_5','des_kmeans_10','des_kmeans_15','des_kmeans_20','des_kmeans_25',
 'size_img','brightness',
 # 'price_to_max_low_mean', 'price_to_max_high_mean', 'price_to_max_medium_mean',
 # 'price_to_max_low_median', 'price_to_max_high_median', 'price_to_max_medium_median']
'img_size_to_max_low',
'img_size_to_max_high', 'img_size_to_max_medium',
'brightness_to_max_low',
'brightness_to_max_high', 'brightness_to_max_medium',
#'address_enc']
'address_enc_2',
'price_week','price_month','price_day',
'avg_price_week', 'avg_price_month', 'avg_price_day',
'street_address_enc_2']
# 'brightness_price']

class cust_regression_vals(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, hd_searches):

        array_of_features_to_use = features_to_use  #'year',

        hd_searches = hd_searches[array_of_features_to_use]
        return hd_searches


class cust_txt_col(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key
    def fit(self, x, y=None):
        return self
    def transform(self, data_dict):
        return data_dict[self.key].apply(str)

class to_array(BaseEstimator, TransformerMixin):
    def fit(self, x, y=None):
        return self
    def transform(self, data_dict):
        return data_dict.toarray()

starting_time = datetime.datetime.now()
print("Starting time")
print(str(starting_time))

def get_shops(filename, fieldname):
    pd_train = pd.read_csv(filename)
    pd_train = pd_train.dropna()
    l = []
    for c in pd_train[fieldname].tolist():
        if len(c.split('(')) > 1:
            l.append(c.split('(')[1].split(')')[0])
    # pd_train['place'] = pd_train['Location'].map(lambda x: x.split('(')[1].split(')')[0])

    # l= pd_train['place'].tolist()
    toret = []
    for i in l:
        try:
            toret.append((float(i.split(',')[1]),float(i.split(',')[0])))
        except:
            pass
    return toret

pd_train = pd.read_json('data/train-formatted.json')
pd_test = pd.read_json('data/test-formatted.json')
id_test = pd_test['listing_id']



num_train = pd_train.shape[0]
df_all = pd.concat((pd_train, pd_test), axis=0, ignore_index=True)

# df_all.loc[(df_all['bedrooms'] == 0) & df_all['description'].str.contains('bed') ,'bedrooms']=1


def change_street(x):

    x = x.lower()
    x = x.replace('st.', 'street')
    x = x.replace('st,', 'street')


def change_features(x):

    if not isinstance(x, basestring):
        return ''

    x = x.lower()

    x = remove_html(x)
    x = x.strip(' ')
    x = x.strip('\t')
    x = x.strip('\n')
    x = x.strip('\r')
    x = re.sub(r'\d+', ' ', x)

    x = x.replace('/', ' ')
    x = x.replace('!', ' ')
    x = x.replace('(', ' ')
    x = x.replace(')', ' ')
    x = x.replace('...', ' ')
    x = x.replace('..', ' ')
    x = x.replace('"', ' ')
    x = x.replace('\'', '')
    x = x.replace('___', ' ')
    x = x.replace('xlarge', 'extra large')


    x = x.replace('pre war', 'prewar')
    x = x.replace('pre-war', 'prewar')

    x = x.replace('post war', 'postwar')
    x = x.replace('post-war', 'postwar')

    x = x.replace('dish washer', 'dishwasher')
    x = x.replace('fire place', 'fireplace')
    x = x.replace('walk in', 'walkin')
    x = x.replace('walk-in', 'walkin')
    x = x.replace('on site', 'onsite')
    x = x.replace('on-site', 'onsite')
    x = x.replace('counter-tops', 'countertops')
    x = x.replace('counter tops', 'countertops')
    x = x.replace('roof-deck', 'roofdeck')
    x = x.replace('roof deck', 'roofdeck')
    x = x.replace('eat in', 'eatin')
    x = x.replace('eat-in', 'eatin')
    x = x.replace('walk up', 'walkup')
    x = x.replace('over-size', 'oversize')
    x = x.replace('over size', 'oversize')
    x = x.replace('low-rise', 'lowrise')
    x = x.replace('low rise', 'lowrise')
    x = x.replace('doormen', 'doorman')
    x = x.replace('door man', 'doorman')
    x = x.replace('door men', 'doorman')
    x = x.replace('wat ', 'water ')
    x = x.replace('wash dryer', 'washer dryer')
    x = x.replace('washer & dryer', 'washer dryer')
    x = x.replace('washer and dryer', 'washer dryer')
    x = x.replace('wi fi', 'wifi')
    x = x.replace('wi-fi', 'wifi')
    x = x.replace('laundri', 'laundry')
    x = x.replace('laundri', 'laundry')

    x = x.replace('pets ok', 'pets allowed')
    x = x.replace('a c', 'air conditioning')

    x = x.replace('high ceilings', 'high ceiling')
    x = x.replace('outdoor areas', 'outdoor space')

    x = x.replace('wifi access', 'wifi')
    x = x.replace('washer in unit', 'washer')

    # x = (" ").join([stemmer.stem(z) for z in x.split(" ")])

    return x


from joblib import Parallel, delayed
import multiprocessing

# what are your inputs, and what operation do you want to

# perform on each input. For example...

inputs = range(10)

def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

def processInput(serie, coord):
    ll = {}
    for x in serie:
        lat = float(x.split('#')[0])
        lon = float(x.split('#')[1])
        ll[x]={}
        tt = []
        for c in coord:
            tt.append(gpxpy.geo.haversine_distance(lat, lon, c[1], c[0]))
        npa = np.asarray(tt)
        if len(npa) < 1:
            print npa
        ll[x]['min'] = np.min(npa)
        ll[x]['1000'] = len(np.where( npa < 1000 )[0])
        ll[x]['150'] = len(np.where( npa < 150 )[0])

    return ll

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def calculate_distances(serie, coord):

    num_cores = 8

    total = len(serie)/num_cores

    # lseries = [serie[:total], serie[total:total*2], serie[total*2:total*3], serie[total*3:total*4], serie[total*4:total*5], serie[total*5:total*6],serie[total*6:total*7],serie[total*7:]]

    print 'starting clacls'
    results = Parallel(n_jobs=8)(delayed(processInput)(i,coord) for i in chunks(serie, 1000))
    print 'all calculated'

    distances1 = merge_dicts(*results)

    return distances1

def distance_to_close_sub1(x, coord, distances):
    return float(distances[x]['min'])

def how_many_under_a_km(x, coord, distances):
    return float(distances[x]['1000'])

def how_many_under_a_150m(x, coord, distances):
    return float(distances[x]['150'])


CONST_LENGTH=1

def calc_features_aprox(x):
    if len(x)<1:
        return CONST_LENGTH
    else:
        return len(re.split(r"\~|;|,|\*|\.|\n", x))+1


def lenPics(x, sep):
    x = x.strip(' ')
    if len(x)<1:
        return CONST_LENGTH
    else:
        return len(x.split(sep))+1


def first_updates(df_all_i):

    df_all_i['length_features'] = df_all_i['features'].map(lambda x: len(x))
    df_all_i['length_desc'] = df_all_i['description'].map(lambda x: len(x))
    df_all_i['word_desc'] = df_all_i['description'].map(lambda x: lenPics(x, ' '))
    df_all_i['word_features'] = df_all_i['features'].map(lambda x: lenPics(x, ' '))
    df_all_i['prize_feature'] = df_all_i["price"]/df_all_i["word_features"]
    df_all_i['prize_desc'] = df_all_i["price"]/df_all_i["word_desc"]

    df_all_i['num_features'] = df_all_i['features'].map(lambda x: calc_features_aprox(x))

    df_all_i['year'] =   df_all_i['created'].map(lambda x: int(x.split(" ")[0].split("-")[0]))

    df_all_i['month'] =   df_all_i['created'].map(lambda x: int(x.split(" ")[0].split("-")[1]))

    df_all_i['day'] =   df_all_i['created'].map(lambda x: int(x.split(" ")[0].split("-")[2]))

    df_all_i['weekday'] =  df_all_i['created'].map(lambda x: datetime.datetime(int(x.split(" ")[0].split("-")[0]),int(x.split(" ")[0].split("-")[1]),int(x.split(" ")[0].split("-")[2]), 0 ,0 ,0).weekday())

    df_all_i['pictures_length'] =   df_all_i['photos'].map(lambda x: lenPics(x, ','))

    # df_all_i.loc[df_all_i['bedrooms']==0, 'bedrooms'] = 0.1
    # df_all_i.loc[df_all_i['bathrooms']==0, 'bathrooms'] = 0.1

    df_all_i["price_be"] = df_all_i["price"]/df_all["bedrooms"]
    df_all_i["price_ba"] = df_all_i["price"]/df_all["bathrooms"]

    df_all_i["price_be_ba"] = df_all_i["price"]/(df_all_i["bedrooms"]+df_all_i["bathrooms"])

    df_all_i['price_sum'] = df_all_i['price'].groupby(df_all_i['building_id']).transform('sum')
    df_all_i['price_count'] = df_all_i['price'].groupby(df_all_i['building_id']).transform('count')
    df_all_i['price_avg_building'] = df_all_i['price_sum'] / df_all_i['price_count']

    df_all_i['bed_sum'] = df_all_i['bedrooms'].groupby(df_all_i['building_id']).transform('sum')
    df_all_i['bed_count'] = df_all_i['bedrooms'].groupby(df_all_i['building_id']).transform('count')
    df_all_i['bed_avg_building'] = df_all_i['bed_sum'] / df_all_i['bed_count']

    df_all_i['bath_sum'] = df_all_i['bathrooms'].groupby(df_all_i['building_id']).transform('sum')
    df_all_i['bath_count'] = df_all_i['bathrooms'].groupby(df_all_i['building_id']).transform('count')
    df_all_i['bath_avg_building'] = df_all_i['bath_sum'] / df_all_i['bath_count']

    df_all_i['building_id_count'] = df_all_i['price'].groupby(df_all_i['building_id']).transform('count')

    df_all_i['price_sum'] = df_all_i['price'].groupby(df_all_i['manager_id']).transform('sum')
    df_all_i['price_count'] = df_all_i['price'].groupby(df_all_i['manager_id']).transform('count')
    df_all_i['price_avg_manager'] = df_all_i['price_sum'] / df_all_i['price_count']

    df_all_i['manager_id_count'] = df_all_i['price'].groupby(df_all_i['manager_id']).transform('count')

    df_all_i['pictures_length_sum'] = df_all_i['pictures_length'].groupby(df_all_i['manager_id']).transform('sum')
    df_all_i['pictures_length_count'] = df_all_i['pictures_length'].groupby(df_all_i['manager_id']).transform('count')
    df_all_i['pictures_length_avg_manager'] = df_all_i['pictures_length_sum'] / df_all_i['pictures_length_count']

    df_all_i['pictures_length_sum'] = df_all_i['pictures_length'].groupby(df_all_i['building_id']).transform('sum')
    df_all_i['pictures_length_count'] = df_all_i['pictures_length'].groupby(df_all_i['building_id']).transform('count')
    df_all_i['pictures_length_avg_building'] = df_all_i['pictures_length_sum'] / df_all_i['pictures_length_count']




    enc = LabelEncoder()
    ohenc = OneHotEncoder()
    df_all_i['building_id_enc'] = enc.fit_transform(df_all['building_id'])

    df_all_i['manager_id_enc'] = enc.fit_transform(df_all['manager_id'])

    # new_created = ['length_features', 'length_desc', 'word_desc', 'word_features', 'prize_feature', 'prize_desc', 'num_features', 'year', 'month', 'day', 'weekday', 'pictures_length', 'price_be', 'price_ba', 'price_be_ba',
    # 'price_sum', 'price_count', 'price_avg_building', 'bed_sum', 'bed_count', 'bed_avg_building', 'bath_avg_building', 'building_id_count', 'price_avg_manager', 'manager_id_count', 'pictures_length_avg_manager',
    # 'pictures_length_avg_building', 'building_id_enc', 'manager_id_enc', 'listing_id']
    #
    # df_all_i[new_created].to_csv('data/train_few_features.csv', encoding='utf-8', index=False)

    return df_all_i

def address_(x):
    x = x.lower()
    x = x.replace('.', '')
    x = x.replace(' street', ' ')
    x = x.replace(' st', ' ')
    x = x.replace(' sq', ' ')
    x = x.replace(' square', ' ')
    x = x.replace(' av', ' ')
    x = x.replace(' avenue', ' ')
    x = x.replace(' court', ' ')
    # x = x.replace('th', '')
    # x = x.replace('nd', '')
    x = x.replace(' uare', ' ')

    # x = x.replace('rd', '')
    x = x.replace(' ockholm', ' stockholm')



    x = x.replace(' enue', ' ')
    x = x.replace('-', '')
    x = x.replace(',', '')
    x = x.strip()
    return x





# df_all_new_features = first_updates(df_all)


import string
def remove_punctuation(x):
    if not isinstance(x, basestring):
        return ''
    for p in string.punctuation:
        x = x.replace(p, ' ')
    return x


def calc_distances(df):


    coord_shops = get_shops('data/shops.csv','Location')
    coord_wineries = get_shops('data/wineries.csv','Location')
    coord_farmers = get_shops('data/farmers.csv','Location Points')

    filee= open('data/ttube.json', 'r').read()
    lt = json.loads(filee)

    sub_coord = []

    for t in lt:
        sub_coord.append((t['station_location']['coordinates'][0],t['station_location']['coordinates'][1]))

    sub_ent_coord = []

    for t in lt:
        sub_ent_coord.append((t['entrance_location']['coordinates'][0],t['entrance_location']['coordinates'][1]))

    df['latitude_str'] = df['latitude'].astype(str)
    df['longitude_str'] = df['longitude'].astype(str)

    df['lat_and_long'] = df[['latitude_str','longitude_str']].apply(lambda x: '#'.join(x), axis=1)

    print 'calculating sub'
    distances2 = calculate_distances(df['lat_and_long'], sub_coord)
    print 'doing closest'
    df['distance_to_close_sub'] =   df['lat_and_long'].map(lambda x: distance_to_close_sub1(x, sub_coord, distances2))
    df['sub_how_many_under_a_km'] =   df['lat_and_long'].map(lambda x: how_many_under_a_km(x, sub_coord, distances2))
    df['sub_how_many_under_a_150m'] =   df['lat_and_long'].map(lambda x: how_many_under_a_150m(x, sub_coord, distances2))
    print 'fin'

    print 'calculating sub'
    distances2 = calculate_distances(df['lat_and_long'], sub_ent_coord)
    print 'doing closest'
    df['distance_to_close_sub_ent'] =   df['lat_and_long'].map(lambda x: distance_to_close_sub1(x, sub_ent_coord, distances2))
    df['sub_ent_how_many_under_a_km'] =   df['lat_and_long'].map(lambda x: how_many_under_a_km(x, sub_ent_coord, distances2))
    df['sub_ent_how_many_under_a_150m'] =   df['lat_and_long'].map(lambda x: how_many_under_a_150m(x, sub_ent_coord, distances2))
    print 'fin'

    print 'calculating shops'
    distances2 = calculate_distances(df['lat_and_long'], coord_shops)
    print 'doing closest'
    df['distance_to_close_shops'] =   df['lat_and_long'].map(lambda x: distance_to_close_sub1(x, coord_shops, distances2))
    df['shops_how_many_under_a_km'] =   df['lat_and_long'].map(lambda x: how_many_under_a_km(x, coord_shops, distances2))
    df['shops_how_many_under_a_150m'] =   df['lat_and_long'].map(lambda x: how_many_under_a_150m(x, coord_shops, distances2))
    print 'fin'

    print 'calculating wineries'
    distances2 = calculate_distances(df['lat_and_long'], coord_wineries)
    print 'doing closest'
    df['distance_to_close_wineries'] =   df['lat_and_long'].map(lambda x: distance_to_close_sub1(x, coord_wineries, distances2))
    df['swineries_how_many_under_a_km'] =   df['lat_and_long'].map(lambda x: how_many_under_a_km(x, coord_wineries, distances2))
    df['wineries_how_many_under_a_150m'] =   df['lat_and_long'].map(lambda x: how_many_under_a_150m(x, coord_wineries, distances2))
    print 'fin'

    print 'calculating farmers'
    distances2 = calculate_distances(df['lat_and_long'], coord_farmers)
    print 'doing closest'
    df['distance_to_close_farmers'] =   df['lat_and_long'].map(lambda x: distance_to_close_sub1(x, coord_farmers, distances2))
    df['farmers_how_many_under_a_km'] =   df['lat_and_long'].map(lambda x: how_many_under_a_km(x, coord_farmers, distances2))
    df['farmers_how_many_under_a_150m'] =   df['lat_and_long'].map(lambda x: how_many_under_a_150m(x, coord_farmers, distances2))
    print 'fin'

    # df_all_new['shops_how_many_under_a_km'].fillna(0, inplace=True)
    # df_all_new['shops_how_many_under_a_150m'].fillna(0, inplace=True)
    # df_all_new['swineries_how_many_under_a_km'].fillna(0, inplace=True)
    # df_all_new['wineries_how_many_under_a_150m'].fillna(0, inplace=True)
    # df_all_new['farmers_how_many_under_a_km'].fillna(0, inplace=True)
    # df_all_new['farmers_how_many_under_a_150m'].fillna(0, inplace=True)
    # df_all_new['distance_to_close_shops'] = df_all_new['distance_to_close_shops'].astype(np.float32)
    # df_all_new['shops_how_many_under_a_km'] = df_all_new['shops_how_many_under_a_km'].astype(np.float32)
    # df_all_new['shops_how_many_under_a_150m'] = df_all_new['shops_how_many_under_a_150m'].astype(np.float32)
    # df_all_new['distance_to_close_wineries']= df_all_new['distance_to_close_wineries'].astype(np.float32)
    # df_all_new['distance_to_close_farmers']= df_all_new['distance_to_close_farmers'].astype(np.float32)
    # df_all_new['swineries_how_many_under_a_km']= df_all_new['swineries_how_many_under_a_km'].astype(np.float32)
    # df_all_new['wineries_how_many_under_a_150m']= df_all_new['wineries_how_many_under_a_150m'].astype(np.float32)
    # df_all_new['farmers_how_many_under_a_km'] = df_all_new['farmers_how_many_under_a_km'].astype(np.float32)
    # df_all_new['farmers_how_many_under_a_150m'] = df_all_new['farmers_how_many_under_a_150m'].astype(np.float32)

    new_created = ['farmers_how_many_under_a_150m','farmers_how_many_under_a_km','wineries_how_many_under_a_150m','swineries_how_many_under_a_km','distance_to_close_farmers','distance_to_close_wineries',
    'shops_how_many_under_a_150m','shops_how_many_under_a_km','distance_to_close_shops', 'distance_to_close_sub', 'sub_how_many_under_a_km', 'sub_how_many_under_a_150m',
    'distance_to_close_sub_ent', 'sub_ent_how_many_under_a_km', 'sub_ent_how_many_under_a_150m', 'listing_id']

    df[new_created].to_csv('data/train_distances.csv', encoding='utf-8', index=False)

    return df
import math
def new_round(df_round):

    df_round['avg_pic_room'] = df_round['pictures_length']/(df_round['bathrooms']+df_round['bedrooms'])
    # df_round['bed_&_bath'] = df_round['bathrooms']+df_round['bedrooms']
    # df_round['bed_or_bath'] = df_round['bedrooms']/(df_round['bathrooms']+df_round['bedrooms'])
    # df_round.loc[df_round['bed_&_bath']==np.inf, 'bed_&_bath'] = -1
    # df_round.loc[df_round['bed_or_bath']==np.inf, 'bed_or_bath'] = -1
    # df_round.loc[df_round['bed_&_bath']==np.nan, 'bed_&_bath'] = -1
    # df_round.loc[df_round['bed_or_bath']==np.nan, 'bed_or_bath'] = -1

    df_round.loc[df_round['avg_pic_room']==np.inf, 'avg_pic_room'] = -1
    # df_round['avg_pic_room'][df_round['avg_pic_room']==np.inf] = np.nan
    df_round['avg_length_features'] = df_round['length_features']/df_round['word_features']
    df_round['avg_length_desc'] = df_round['length_desc']/df_round['word_desc']



    df_round['week'] =  df_round['created'].map(lambda x: datetime.datetime(int(x.split(" ")[0].split("-")[0]),int(x.split(" ")[0].split("-")[1]),int(x.split(" ")[0].split("-")[2]), 0 ,0 ,0).isocalendar()[1])

    listing_months = df_round['listing_id'].groupby(df_round['month']).transform('count')
    listing_days = df_round['listing_id'].groupby(df_round['day']).transform('count')
    listing_weeks = df_round['listing_id'].groupby(df_round['week']).transform('count')

    price_months = df_round['price'].groupby(df_round['month']).transform('sum')
    price_days = df_round['price'].groupby(df_round['day']).transform('sum')
    price_weeks = df_round['price'].groupby(df_round['week']).transform('sum')

    avg_months = price_months/listing_months
    avg_weeks = price_weeks/listing_weeks
    avg_days = price_days/listing_days

    df_round['avg_price_week'] = df_round['week'].map(lambda x: avg_weeks[x])
    df_round['avg_price_month'] = df_round['month'].map(lambda x: avg_months[x])
    df_round['avg_price_day'] = df_round['day'].map(lambda x: avg_days[x])

    df_round['price_week'] = df_round['price']/df_round['avg_price_week']
    df_round['price_month'] = df_round['price']/df_round['avg_price_month']
    df_round['price_day'] = df_round['price']/df_round['avg_price_day']


    # df_round['price'] = df_round['price'].map(lambda x: x*math.log(x))


    df_round_clean = df_round[df_round['price'] > 1000]
    median_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean['interest_level'])['price'].median()
    mean_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean['interest_level'])['price'].mean()
    count_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean['interest_level'])['price'].count()
    sum_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean['interest_level'])['price'].sum()
    avg_prices = sum_prices / count_prices

    size_img_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean['interest_level'])['size_img'].sum()
    avg_size_img = size_img_prices / count_prices
    brightness_img_prices = df_round_clean.iloc[:num_train].groupby(df_round_clean['interest_level'])['brightness'].sum()
    avg_brightness = size_img_prices / count_prices

    # print count_prices
    # print sum_prices
    # print avg_prices

    df_round['img_size_to_max_low'] = avg_size_img['low']/df_round['price']
    df_round['img_size_to_max_high'] = avg_size_img['high']/df_round['price']
    df_round['img_size_to_max_medium'] = avg_size_img['medium']/df_round['price']

    df_round['brightness_to_max_low'] = avg_brightness['low']/df_round['price']
    df_round['brightness_to_max_high'] = avg_brightness['high']/df_round['price']
    df_round['brightness_to_max_medium'] = avg_brightness['medium']/df_round['price']

    df_round['brightness_price'] = df_round['brightness']/df_round['price']

    df_round['price_to_max_low'] = avg_prices['low']/df_round['price']
    df_round['price_to_max_high'] = avg_prices['high']/df_round['price']
    df_round['price_to_max_medium'] = avg_prices['medium']/df_round['price']


    df_round['price_to_max_low_mean'] = mean_prices['low']/df_round['price']
    df_round['price_to_max_high_mean'] = mean_prices['high']/df_round['price']
    df_round['price_to_max_medium_mean'] = mean_prices['medium']/df_round['price']


    df_round['price_to_max_low_median'] = median_prices['low']/df_round['price']
    df_round['price_to_max_high_median'] = median_prices['high']/df_round['price']
    df_round['price_to_max_medium_median'] = median_prices['medium']/df_round['price']

    df_round['hour'] = df_round['created'].map(lambda x: int(x.split(" ")[1].split(":")[0]))
    df_round['am'] = df_round['hour'].map(lambda x: int(x>=0 and x < 12))
    df_round['pm'] = df_round['hour'].map(lambda x: int(x>=12 and x <24))
    df_round['exclamation'] = df_round['description'].map(lambda x: x.count('!'))
    df_round['interrognat'] = df_round['description'].map(lambda x: x.count('?'))
    df_round['interrognat_exc'] =df_round['interrognat'] + df_round['exclamation']

    enc = LabelEncoder()
    df_round['address_enc'] = enc.fit_transform(df_round['display_address_new'])

    return df_round

from sklearn import preprocessing

def zeroIfnan(x, val):
    if math.isnan(x):
        return val
    else:
        return x

def zeroIfnanS(x, val):
    if isinstance(x, basestring):
        return x
    else:
        return val

def getnansout(df, col, val):
    df.loc[df[col]==np.nan, col] =val
    df[col] = df[col].map(lambda x: zeroIfnan(x, val))
    df.loc[df[col]==np.inf, col] = val

    return df

def dokmeans(df):

    # for kkk in [ 'price_be_ba', 'price_be', 'price_ba']:
    #
    #     df = getnansout(df, kkk, 0.0)
    #
    #
    from sklearn.cluster import KMeans
    from collections import Counter
    #
    # for k in [5, 10 ,15, 20, 25, 30]:
    #     print k
    #     kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[[ 'price_be_ba', 'price_be', 'price_ba', 'price']])
    #     df['varkmeans'+str(k)] = kmeans.predict(df[[ 'price_be_ba', 'price_be', 'price_ba', 'price']])





    #
    # df.loc[df['brightness']==np.nan, 'brightness'] = 0.0
    # df['brightness'] = df['brightness'].map(lambda x: zeroIfnan(x))
    # df.loc[df['brightness']==np.inf, 'brightness'] = 0.0
    #
    # min_max_scaler = preprocessing.MinMaxScaler()
    # df['brightness'] = min_max_scaler.fit_transform(df['brightness'])
    # # df_normalized = pd.DataFrame(np_scaled)
    # # df_normalized
    #
    # print
    # print np.isfinite(df[['brightness','price','latitude','longitude']].sum())
    # print np.isfinite(df[['brightness','price','latitude','longitude']].all())
    #
    # print df['brightness'].min()
    # for k in [5, 10 ,15, 20, 25, 30]:
    #     print k
    #     kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[['brightness','price']])
    #     df['pkmeans'+str(k)] = kmeans.predict(df[['brightness','price']])

    for k in [5, 10 ,15, 20, 25, 30]:
        print k
        kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[['latitude','longitude']])
        df['skmeans'+str(k)] = kmeans.predict(df[['latitude','longitude']])


        listing_weeks = df['listing_id'].groupby(df['skmeans'+str(k)]).transform('count')
        price_weeks = df['price'].groupby(df['skmeans'+str(k)]).transform('sum')
        avg_weeks = price_weeks/listing_weeks

        df['skmeans'+str(k)+'avg_price'] = df['skmeans'+str(k)].map(lambda x: avg_weeks[x])
        df['skmeans'+str(k)+'price_week'] = df['price']/df['skmeans'+str(k)+'avg_price']
        # features_to_use.append('skmeans'+str(k)+'avg_price')
        # features_to_use.append('skmeans'+str(k)+'price_week')


    for k in [5, 10 ,15, 20, 25, 30]:
        print k
        kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[['latitude','longitude', 'price', 'bathrooms', 'bedrooms']])
        df['kmeans'+str(k)] = kmeans.predict(df[['latitude','longitude','price', 'bathrooms', 'bedrooms']])
        # preds =  kmeans.predict(df.iloc[num_train:][['latitude','longitude','price', 'bathrooms', 'bedrooms']])

        df['kmeans'+str(k)+'avg_price'] = df['skmeans'+str(k)].map(lambda x: avg_weeks[x])
        df['kmeans'+str(k)+'price_week'] = df['price']/df['kmeans'+str(k)+'avg_price']
        # features_to_use.append('kmeans'+str(k)+'avg_price')
        # features_to_use.append('kmeans'+str(k)+'price_week')

    for k in [5, 10 ,15, 20, 25, 30]:
        print k
        kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[['word_desc', 'word_features', 'prize_feature', 'prize_desc','length_desc','length_features', 'pictures_length']])
        df['wkmeans'+str(k)] = kmeans.predict(df[['word_desc', 'word_features', 'prize_feature', 'prize_desc','length_desc','length_features', 'pictures_length']])


    # for k in [5, 10 ,15, 20, 25]:
    #     print k
    #     kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[['year', 'month', 'day', 'weekday', 'hour']])
    #     df['tkmeans'+str(k)] = kmeans.predict(df[['year', 'month', 'day', 'weekday', 'hour']])

    with_this= ['latitude','longitude','price', 'bathrooms', 'bedrooms', 'price_to_max_low', 'price_to_max_high', 'price_to_max_medium','length_desc','length_features', 'manager_id_enc', 'building_id_enc']
    # 'lda10','lda20','lda30','lda40','lda50','lda80','lda100']
    for k in [5, 10 ,15, 20, 25, 30]:
        print k
        kmeans = KMeans(n_clusters=k, random_state=2017, n_jobs=-1).fit(df[with_this])
        df['bkmeans'+str(k)] = kmeans.predict(df[with_this])



    return df



# df_with_distances = calc_distances(df_all_new_features)
#
# sys.exit(0)
from bs4 import BeautifulSoup
def remove_html(x):
    cleantext = BeautifulSoup(x).text
    return cleantext

# df_few = pd.read_csv(open('data/train_few_features.csv', 'r'), encoding='utf-8')
df_dist = pd.read_csv(open('data/train_distances.csv', 'r'), encoding='utf-8')
df_lda = pd.read_csv(open('data/train_description_groups_with_data.csv', 'r'), encoding='utf-8')
df_kmeans = pd.read_csv(open('data/train_description_kmeans2_groups_with_data.csv', 'r'), encoding='utf-8')
df_kmeans2 = pd.read_csv(open('data/train_description_kmeans_groups_with_data.csv', 'r'), encoding='utf-8')
df_kmeans3 = pd.read_csv(open('data/train_description_kmeans3_groups_with_data.csv', 'r'), encoding='utf-8')
df_kmeans5 = pd.read_csv(open('data/train_description_kmeans5_groups_with_data.csv', 'r'), encoding='utf-8')
df_image_size = pd.read_csv(open('data/train_image_size.csv', 'r'), encoding='utf-8')
df_image_color = pd.read_csv(open('data/train_image_colors.csv', 'r'), encoding='utf-8')

# df_all_new = pd.merge(df_all, df_few, how='left', on='listing_id')
df_all_new = pd.merge(df_all, df_dist, how='left', on='listing_id')
df_all_new = pd.merge(df_all_new, df_lda, how='left', on='listing_id')
df_all_new = pd.merge(df_all_new, df_kmeans, how='left', on='listing_id')
df_all_new = pd.merge(df_all_new, df_kmeans2, how='left', on='listing_id')
df_all_new = pd.merge(df_all_new, df_kmeans3, how='left', on='listing_id')
df_all_new = pd.merge(df_all_new, df_kmeans5, how='left', on='listing_id')
df_all_new = pd.merge(df_all_new, df_image_size, how='left', on='listing_id')
df_all_new = pd.merge(df_all_new, df_image_color, how='left', on='listing_id')

for col in ['color1', 'color2', 'color3']:
    # df_all_new.loc[df_all_new[col]==float('Na'), col] = '#0'
    df_all_new[col] = df_all_new[col].map(lambda x: zeroIfnanS(x,'#0'))
    print df_all_new[col]
    df_all_new[col] = df_all_new[col].map(lambda x: x[1:])
    df_all_new[col] = df_all_new[col].map(lambda x: int(x, 16))

    # features_to_use.append(col)

print features_to_use

# df_all_new=doKneighbours(df_all_new)

def address_1(x):
    if not isinstance(x, basestring):
        return ''
    x = x.lower()
    x = x.replace(' e ', ' east ')
    x = x.replace(' w ', ' west ')
    x = x.replace(' s ', ' south ')
    x = x.replace(' n ', ' north ')
    x = re.sub(r'^e ', 'east ', x)
    x = re.sub(r'^w ', 'west ', x)
    x = re.sub(r'^s ', 'south ', x)
    x = re.sub(r'^n ', 'north ', x)
    x = x.replace(' st.', ' street')
    x = x.replace(' st ', ' street')
    x = x.replace(' ave.', ' avenue')
    x = x.replace(' pl.', ' place')
    x = x.replace(' pl', ' place')

    x = re.sub(r' sq$', ' square', x)
    x = re.sub(r' st$', ' street', x)
    x = re.sub(r' ave$', ' avenue', x)

    x = x.replace('first', '1st')
    x = x.replace('second', '2nd')
    x = x.replace('third', '3rd')
    x = x.replace('fourth', '4th')
    x = x.replace('fifth', '5th')
    x = x.replace('sixth', '5th')
    x = x.replace('seventh', '7th')
    x = x.replace('eighth', '8th')
    x = x.replace('ninth', '9th')
    x = x.replace('tenth', '10th')
    x = x.replace('eleventh', '11th')
    x = x.replace('twelveth', '12th')

    x = x.replace('.', '')
    x = x.replace(',', ' ')
    x = x.strip()

    return x

df_all_new['display_address_new'] = df_all_new['display_address'].map(lambda x: address_1(x))
df_all_new['street_address_new'] = df_all_new['street_address'].map(lambda x: address_1(x))
# df_all_new['street_address_new'] = df_all_new['street_address'].map(lambda x: address_1(x))
# df_all_new['new_features_desc'] = df_all_new['features'] + " - " +df_all_new['description']
#
df_all_new['new_features'] = df_all_new['features'].map(lambda x: change_features(x))
# df_all_new['new_features_desc'] = df_all_new['new_features_desc'].map(lambda x: remove_punctuation(x))
df_all_new['new_features'] = df_all_new['new_features'].map(lambda x: remove_punctuation(x))

def isEast(x):
    if 'east' in x:
        return 1
    elif 'west' in x:
        return 0
    else:
        return np.nan

df_all_new['east'] = df_all_new['display_address_new'].map(lambda x: isEast(x))

# print df_all_new[['display_address_new','street_address']]

# df_all_new['display_address_new_2'] = df_all_new['display_address_new'].map(lambda x: " ".join(x.split(' ')[-2:]))
# print df_all_new['display_address_new_2']
enc = LabelEncoder()
df_all_new['address_enc_2'] = enc.fit_transform(df_all_new['display_address_new'])

df_all_new['street_address_enc_2'] = enc.fit_transform(df_all_new['street_address_new'])


print np.unique(df_all_new['address_enc_2'])

# sys.exit(0)
df_all_new = first_updates(df_all_new)
df_all_new = new_round(df_all_new)
df_all_new=dokmeans(df_all_new)





from numpy import inf
# df_all_new['price_be'][df_all_new['price_be'] == inf] = 0
# df_all_new['price_ba'][df_all_new['price_ba'] == inf] = 0



df_train = df_all_new.iloc[:num_train]
df_test = df_all_new.iloc[num_train:]

print features_to_use

print df_train.shape
df_train = df_train[df_train['price'] > 1000]
print df_train.shape

# df_train = df_train[df_train['latitude'] > 40]


# for i in features_to_use:
#     print i
#     print df_train[i].isnull().sum()
#
# print

# dfdf= df_train[df_train['interest_level'].str.contains("low")]
# print dfdf[['listing_id', 'display_address', 'street_address', 'price']]
# sys.exit(0)
# df_train = sklearn.utils.shuffle(df_train)
y_train = df_train['interest_level']
X_train = df_train[:]
X_test = df_test[:]





enc = LabelEncoder()
ohenc = OneHotEncoder()
y_train = enc.fit_transform(y_train)

# def il(x):
#     if x=='low':
#         return 0
#     if x=='medium':
#         return 1
#     if x=='high':
#         return 2
#
# y_train = y_train.map(lambda x: il(x))

pca = PCA(n_components=10)
tsvd = TruncatedSVD(n_components=100, random_state=2016)
gbm = xgb.XGBClassifier()
cvect = CountVectorizer(ngram_range=(1, 2), stop_words='english')
cvectst = CountVectorizer(ngram_range=(1, 2), max_features=4000)
cvectsts = CountVectorizer(ngram_range=(1, 3))
tfidf = TfidfVectorizer(ngram_range=(1, 2), stop_words='english')
clf = Pipeline([
    ('union', FeatureUnion(transformer_list=[
        ('cst', cust_regression_vals()),
        ('txt1', Pipeline([
            ('s1', cust_txt_col(key='new_features')),
            ('tfidf1', cvectst)
            # ,('tsvd1', tsvd)
            ])
        ),

        # ,('txt2', Pipeline([
        #     ('s2', cust_txt_col(key='new_features_desc')),
        #     ('tfidf2', tfidf)
        #     # ,('tsvd2', tsvd)
        #     ])
        # )
        # ('txt3', Pipeline([
        #     ('s3', cust_txt_col(key='display_address_new')),
        #     ('cvectst3', tfidf)
        #     ])
        # )
        # ,('txt4', Pipeline([
        #     ('s4', cust_txt_col(key='street_address_new')),
        #     ('cvectst4', tfidf)
        #     ])
        # )
    ])),
    ('model', gbm)
    ])


# param_grid = {
#     'model__n_estimators': [580,590, 600, 610], #400
#     'model__max_depth': [ 6], #7
#     'model__learning_rate': [0.05],
#     'model__base_score': [0.8, 0.75],
#     'model__objective': ['multi:softprob'],
#     'model__subsample': [0.8, 0.85],
#     'model__colsample_bytree': [0.9],
#     'model__colsample_bylevel': [0.7, 0.75],
#     # 'model__min_child_weight': [6]
#     'model__min_child_weight': [10],
#     'model__gamma': [0.1, 0.15],#, 0.2],
#     'model__nthread':[3]
# }

param_grid = {
    'model__n_estimators': [500], #400
    'model__max_depth': [ 6], #7
    'model__learning_rate': [0.05],
    'model__base_score': [0.75],
    'model__objective': ['multi:softprob'],
    'model__subsample': [0.8],
    'model__colsample_bytree': [0.9],
    'model__colsample_bylevel': [0.75],
    # 'model__min_child_weight': [6]
    'model__min_child_weight': [10],
    'model__gamma': [0.1],#, 0.2],
    'model__nthread':[3]
}

#
# param_grid = {
#     'model__n_estimators': [50],
#     'model__max_depth': [ 2],
#     'model__learning_rate': [0.05],
#     'model__base_score': [0.9],
#     'model__objective': ['multi:softprob'],
#     'model__subsample': [0.8],
#     'model__colsample_bytree': [0.9],
#     # 'model__min_child_weight': [6]
#     'model__min_child_weight': [10]
# }

# array_of_features_to_use = ['bathrooms', 'bedrooms', 'latitude', 'longitude', 'price', 'length_features', 'length_desc']
#
# X_train = X_train[array_of_features_to_use]
# print(X_train)

# print(X_train)
MLL = make_scorer(log_loss, greater_is_better=False, labels=np.unique(y_train), needs_proba=True)


model = grid_search.GridSearchCV(estimator = clf, param_grid = param_grid, n_jobs = 2, cv = 2, verbose = 20, scoring=MLL, fit_params={'model__eval_metric':'mlogloss'})#, 'model__early_stopping_rounds':40, 'model__eval_metric':"auc"}) #, fit_params={'model__early_stopping_rounds': 50, 'model__eval_metric': MLL})
model.fit(X_train, y_train)

print("Best parameters found by grid search:")
print(model.best_params_)
print("Best CV score:")
print(model.best_score_)

best_score = model.best_score_

est = model.best_estimator_
print('Predicting test data !!!')
probs = est.predict_proba(X_test)
# print(probs)
print(est._final_estimator.booster().get_fscore())
fscores = est._final_estimator.booster().get_fscore()
fscores_sorted = sorted(fscores.keys())
for key in fscores_sorted:
    print("{} : {}".format(key, fscores[key]))
print('Classes')
print(est.classes_)
print ('Encoder')
print(enc.classes_)
print(enc.inverse_transform(est.classes_))
array_classes = enc.inverse_transform(est.classes_)
indices_low = [i for i, s in enumerate(array_classes) if 'low' in s]
indices_medium = [i for i, s in enumerate(array_classes) if 'medium' in s]
indices_high = [i for i, s in enumerate(array_classes) if 'high' in s]

# print(id_test.shape)
print(indices_high)
pd.DataFrame({"listing_id": id_test, "high": probs[:,indices_high[0]], "medium": probs[:,indices_medium[0]], "low": probs[:,indices_low[0]]}).to_csv('submission{}.csv'.format(str(best_score)),index=False)
print 'File saved'
print 'submission{}.csv'.format(str(best_score))

# f = open("submission.csv", "w")
# f.write("listing_id,high,medium,low\n")
# for i in xrange(len(ds2)):
#     f.write(ds2.index[i] + "|" + str(ds2["count"][i]) + "|" + str(ds2["in title"][i]) + "|" + str(ds2["in brand"][i]) + "|" + str(ds2["in prod"][i]) + "\n")
# f.close()
# print("--- Word List Created: %s minutes ---" % round(((time.time() - start_time)/60), 2))

ending_time = datetime.datetime.now()
print("Ending time")
print(str(ending_time))
print()

elapsedTime = starting_time - ending_time
elapsed = divmod(elapsedTime.total_seconds(), 60)
print(elapsed)
