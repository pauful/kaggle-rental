from PIL import Image, ImageStat
import math
import os, sys

with open('data/train_image_size.csv', 'w') as f:
    f.write('listing_id,size_img,brightness\n')
    for dd in [x for x in os.listdir('images/images')]:
        total = 0
        num = 0

        brightness = 0
        for im in os.listdir('images/images/'+dd):

            if os.path.isdir('images/images/'+dd+'/'+im) or not im.endswith('.jpg'):
                continue

            im=Image.open('images/images/'+dd+'/'+im)
            total = total + im.size[0]*im.size[1]
            num = num +1
            try:
                stat = ImageStat.Stat(im)
                r,g,b = stat.mean
                brightness = brightness + math.sqrt(0.241*(r**2) + 0.691*(g**2) + 0.068*(b**2))
            except:
                pass
        avg = 0.0
        if num != 0:
            avg = total / num

        avg_b = 0.0
        if num != 0:
            avg_b = brightness / num

        f.write(str(dd)+','+str(avg)+','+str(avg_b)+'\n')

        # sys.exit(0)

# im=Image.open(filepath)
# im.size # (width,height) tuple
