import datetime
import pandas as pd
train_df = pd.read_json('data/train-formatted.json')
test_df = pd.read_json('data/test-formatted.json')
num_train = train_df.shape[0]
df = pd.concat([train_df, test_df], 0)

df["created"] = pd.to_datetime(df["created"])

mintime = df['created'].min()



def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0

mintime = unix_time_millis(mintime)

df["millis"] = df["created"].map(lambda x: unix_time_millis(x))


df['bedrooms'] = df['bedrooms'].fillna(-1)
df['bedrooms'] = df['bedrooms'].apply(str)

# df_train = df.iloc[:num_train]

# agg_prices_time = (df.
#       sort_values("millis").
#       groupby(['bedrooms',"millis"]).
#       agg(sum)['price'].
#       reset_index("millis"))

agg_prices_time = pd.read_csv('agg_prices_time.csv')
import sys
def cc(x, agg):

    suma =  agg[(agg['bedrooms'] == int(x.split('#')[0])  & (agg_prices_time['millis'] < float(x.split('#')[1]))) ].tail(10)['price'].sum()
    count =  agg[(agg['bedrooms'] == int(x.split('#')[0])  & (agg_prices_time['millis'] < float(x.split('#')[1]))) ].tail(10)['price'].count()


    return suma / count

# print agg[(agg.index == int(0)) & (agg['millis'] < float(x.split('#')[1]))]

df['millis_str'] = df['millis'].apply(str)
# df['bedrooms'] = df['bedrooms'].fillna(-1)
df['mgr_list'] = df['bedrooms'] + '#'+ df['millis_str']

# df['high_mgr_list_sum']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == int(x.split('#')[0])) & (agg_prices_time['millis'] < float(x.split('#')[1]))].head(10)['price'].sum())
# df['high_mgr_list_sum']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == int(x.split('#')[0]))].head(10)['price'].sum())
df['bed_list_avg']=df['mgr_list'].map(lambda x: cc(x,agg_prices_time))#[(agg_prices_time.index == int(x.split('#')[0]))].head(10)['price'].sum())
# print df
# df['high_mgr_list_count']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == int(x.split('#')[0])) & (agg_prices_time['millis'] < float(x.split('#')[1]))].head(10)['price'].count())
# print df
# df['bed_list_avg'] = df['high_mgr_list_sum'] /df['high_mgr_list_count']

df[['listing_id','bed_list_avg']].to_csv('data/train_acc_bed_price1.csv', encoding='utf-8', index=False)
