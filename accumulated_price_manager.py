import datetime
import pandas as pd
train_df = pd.read_json('data/train-formatted.json')
test_df = pd.read_json('data/test-formatted.json')

df = pd.concat([train_df, test_df], 0)

df["created"] = pd.to_datetime(df["created"])

mintime = df['created'].min()



def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0

mintime = unix_time_millis(mintime)

df["millis"] = df["created"].map(lambda x: unix_time_millis(x))

agg_prices_time = (df.
      sort_values("millis").
      groupby(['manager_id',"millis"]).
      agg(sum)['price'].
      reset_index("millis"))

df['millis_str'] = df['millis'].apply(str)
df['mgr_list'] = df['manager_id'] + '#'+ df['millis_str']

df['mgr_list_count']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == x.split('#')[0]) & (agg_prices_time['millis'] <= float(x.split('#')[1]))]['price'].count())
df['mgr_list_sum']=df['mgr_list'].map(lambda x: agg_prices_time[(agg_prices_time.index == x.split('#')[0]) & (agg_prices_time['millis'] <= float(x.split('#')[1]))]['price'].sum())
df['mgr_list_avg'] = df['mgr_list_sum'] /df['mgr_list_count']

df[['listing_id','mgr_list_count','mgr_list_sum', 'mgr_list_avg']].to_csv('data/train_acc_price_mgr.csv', encoding='utf-8', index=False)
