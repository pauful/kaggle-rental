#!/usr/bin/python
# -*- coding: ISO-8859-1 -*-
# vim: set fileencoding=ISO-8859-1 :
import gpxpy.geo
import time, datetime, json
start_time = time.time()
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn import grid_search
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.decomposition import TruncatedSVD, RandomizedPCA
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics import  make_scorer
from  sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import StratifiedKFold
from nltk.stem.porter import *
stemmer = PorterStemmer()
import re
#import enchant
import random
random.seed(2016)
import unicodedata
from sklearn.feature_selection import SelectKBest, f_classif, f_regression
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import xgboost as xgb
import sklearn.utils
from sklearn.utils import check_array, check_consistent_length
from sklearn.decomposition import PCA

import sys
reload(sys)
sys.setdefaultencoding('utf8')

features_to_use = ['bathrooms', 'bedrooms', 'price', 'length_features', 'length_desc',
'pictures_length', 'year', 'month', 'day', 'weekday', 'price_be', 'price_ba',  'num_features', 'word_desc', 'word_features', 'prize_feature', 'prize_desc', 'distance_to_close_sub',
'price_be_ba', 'manager_id_enc', 'manager_id_enc', 'price_avg_manager', 'price_avg_building', 'manager_id_count', 'building_id_count', 'distance_to_close_sub_ent',
'sub_how_many_under_a_km', 'sub_how_many_under_a_150m', 'sub_ent_how_many_under_a_km', 'sub_ent_how_many_under_a_150m',
'pictures_length_avg_building', 'pictures_length_avg_manager', 'distance_to_close_shops',  'shops_how_many_under_a_km', 'shops_how_many_under_a_150m',
'distance_to_close_wineries', 'swineries_how_many_under_a_km', 'wineries_how_many_under_a_150m', 'distance_to_close_farmers', 'farmers_how_many_under_a_km', 'farmers_how_many_under_a_150m',
'latitude', 'longitude']

pd_train = pd.read_json('data/train-formatted.json')
pd_test = pd.read_json('data/test-formatted.json')
id_test = pd_test['listing_id']


num_train = pd_train.shape[0]
df_all = pd.concat((pd_train, pd_test), axis=0, ignore_index=True)

df_all['bathrooms'].loc[df_all['bathrooms'] == 0] = -1
df_all['bedrooms'].loc[df_all['bedrooms'] == 0] = -1
df_all['average'] = df_all['bedrooms'] / df_all['price']
# df_all['average'] = (df_all['bathrooms'] + df_all['bedrooms'])/ df_all['price']
df_all['bed_price_beta'] = (df_all['bedrooms'] +495.30204503328309) / (df_all['price'] +495.30204503328309 + 122217.16212746507 )
# print df_all[['price','bedrooms','average', 'bath_price_beta']]
maxn =  df_all['bed_price_beta'].max()
df_all['bed_price_beta'] = df_all['bed_price_beta'].map(lambda x: x / maxn)
print df_all[['price','bedrooms','average', 'bed_price_beta']]

df_all['bed_price_beta'] = df_all['price']/df_all['bedrooms']
df_all[['listing_id', 'bed_price_beta']].to_csv('data/price_bed_beta.csv')
# df_all =df_all[df_all['average'] < 0.002]
import sys; sys.exit(0)


import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt



h = sorted(df_all['average'])

fig = plt.figure()
ax = fig.add_subplot(111)

numBins = 80
ax.hist(h,numBins,color='green',alpha=0.8, normed=True)

from scipy.stats import beta

print beta.fit(h)
import sys; sys.exit(0)

rv = beta(3.783563248634997, 34042.232044668846, 3.4848834268425692e-07, 4.8335897207252154)
ax.plot(h, rv.pdf(h), lw=2)

plt.show()
