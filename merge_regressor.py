import pandas as pd

pd1 = pd.read_csv('submission-kaggle-regressor0.429773241275.csv')
pd2 = pd.read_csv('submission-0.539330857006.csv')

pdd = pd.merge(pd1, pd2, how='left', on='listing_id')

def change(x, m1, m2):
    if x[m1] > 0.5:
        return x[m1]*0.60 + x[m2]*0.4
    else:
        return x[m2]

pdd['lowl']=pdd['low']
pdd['mediuml']=pdd.apply(lambda x: change(x,'medium1','medium'),axis=1)
pdd['highl']=pdd.apply(lambda x: change(x,'high1','high'),axis=1)

pdd[['listing_id','lowl','mediuml','highl']].to_csv('submission-merge-regressor.csv', index=False)
