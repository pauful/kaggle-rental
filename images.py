import pandas as pd
import sys
from Levenshtein import distance
import re
from collections import defaultdict
from nltk.stem.porter import *
stemmer = PorterStemmer()
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

pd_train = pd.read_json('data/train-formatted.json')
pd_test = pd.read_json('data/test-formatted.json')

pd_all = pd.concat([pd_train, pd_test])



image = '7151324_3e9b733131997de86b20a33bc7cb75d5.jpg'

from resnet50 import ResNet50
from vgg16 import VGG16
from keras.preprocessing import image
from imagenet_utils import preprocess_input, decode_predictions
import sys
import numpy as np

#model = ResNet50(weights='imagenet')
model = VGG16(weights='imagenet')

img_path = image
img = image.load_img(img_path, target_size=(224, 224))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)
x = preprocess_input(x)

preds = model.predict(x)
print('Predicted:', decode_predictions(preds))


