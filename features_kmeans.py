import numpy as np
import pandas as pd
import sys
from Levenshtein import distance
import re
from collections import defaultdict
from nltk.stem.porter import *
stemmer = PorterStemmer()
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

pd_train = pd.read_json('data/train-formatted.json')
pd_test = pd.read_json('data/test-formatted.json')

pd_all = pd.concat([pd_train, pd_test])

from bs4 import BeautifulSoup
def remove_html(x):
    cleantext = BeautifulSoup(x).text
    return cleantext

# from collections import Counter
# print Counter(pd_train[pd_train['description'].str.contains('\?')]['interest_level'])
# sys.exit(0)
from nltk.corpus import stopwords
def change_features(x):


    x = x.lower()

    x = remove_html(x)

    x = x.strip(' ')
    x = x.strip('\t')
    x = x.strip('\n')
    x = x.strip('\r')
    x = re.sub(r'\d+', ' ', x)

    x = x.replace('/', ' ')
    x = x.replace('!', ' ')
    x = x.replace('(', ' ')
    x = x.replace(')', ' ')
    x = x.replace('...', ' ')
    x = x.replace('..', ' ')
    x = x.replace('"', ' ')
    x = x.replace('\'', '')
    x = x.replace('___', ' ')
    x = x.replace('!', ' ')
    x = x.replace('?', ' ')
    x = x.replace('&', ' and ')
    x = x.replace('\r', ' ')
    x = x.replace('\n', ' ')
    x = x.replace('\r\r', ' ')
    x = x.replace('<U+0082>', ' ')
    x = x.replace('<U+0083>', ' ')
    x = x.replace('<U+009D>', ' ')
    x = x.replace('<U+2028>', ' ')

    x = x.replace('xlarge', 'extra large')


    x = x.replace('pre war', 'prewar')
    x = x.replace('pre-war', 'prewar')

    x = x.replace('post war', 'postwar')
    x = x.replace('post-war', 'postwar')

    x = x.replace('dish washer', 'dishwasher')
    x = x.replace('fire place', 'fireplace')
    x = x.replace('walk in', 'walkin')
    x = x.replace('walk-in', 'walkin')
    x = x.replace('on site', 'onsite')
    x = x.replace('on-site', 'onsite')
    x = x.replace('counter-tops', 'countertops')
    x = x.replace('counter tops', 'countertops')
    x = x.replace('roof-deck', 'roofdeck')
    x = x.replace('roof deck', 'roofdeck')
    x = x.replace('eat in', 'eatin')
    x = x.replace('eat-in', 'eatin')
    x = x.replace('walk up', 'walkup')
    x = x.replace('over-size', 'oversize')
    x = x.replace('over size', 'oversize')
    x = x.replace('low-rise', 'lowrise')
    x = x.replace('low rise', 'lowrise')
    x = x.replace('doormen', 'doorman')
    x = x.replace('door man', 'doorman')
    x = x.replace('door men', 'doorman')
    x = x.replace('wat ', 'water ')
    x = x.replace('wash dryer', 'washer dryer')
    x = x.replace('washer & dryer', 'washer dryer')
    x = x.replace('washer and dryer', 'washer dryer')
    x = x.replace('wi fi', 'wifi')
    x = x.replace('wi-fi', 'wifi')
    x = x.replace('laundri', 'laundry')
    x = x.replace('laundri', 'laundry')

    x = x.replace('pets ok', 'pets allowed')
    x = x.replace('a c', 'air conditioning')

    x = x.replace('high ceilings', 'high ceiling')
    x = x.replace('outdoor areas', 'outdoor space')

    x = x.replace('wifi access', 'wifi')
    x = x.replace('washer in unit', 'washer')




    stop = stopwords.words('english')
    x = (" ").join([z for z in x.split(" ") if z not in stop])

    x = re.sub(r';|,|\-|\*|\~|\.|\||:', ' ', x)
    x = x.strip()

    x = x.replace('www', ' ')
    x = x.replace(' com ', ' ')
    x= re.sub(' +',' ',x)
    x = (" ").join([stemmer.stem(z) for z in x.split(" ")])
    x = x.encode("utf-8")

    return x


#######################################

# Create dataset



# pd_all['description'] =  pd_all['description'].map(lambda x: change_features(x))
# pd_all['description'] = pd_all['description'].replace(np.nan,'', regex=True)
# pd_all['description'].fillna('', inplace=True)
# pd_all['description'] = pd_all['description'].astype(str)
#
# print  pd_all['description']
#
# pd_all[['listing_id','description']].to_csv('data/train_description_groups.csv', encoding='utf-8', index=False, quotechar="\"")
#
# print 'done'
##########################################
pd_all = pd.read_csv(open('data/train_description_groups.csv', 'r'), encoding='utf-8')
pd_all['description'].fillna('', inplace=True)
pd_all['description'] = pd_all['description'].map(lambda x: x.encode('utf-8'))
pd_all['description'] = pd_all['description'].astype(str)

from sklearn.feature_extraction.text import TfidfVectorizer

tfidf = TfidfVectorizer(strip_accents='unicode', ngram_range=(1,2), stop_words='english', max_df=0.95, min_df=0.05)

data = tfidf.fit_transform(pd_all['description'])
from sklearn.cluster import MiniBatchKMeans
for k in [5,10, 15, 20, 25]:
    print k
    kmeans = MiniBatchKMeans(init='k-means++', n_clusters=k, batch_size=100,
                      n_init=50, max_no_improvement=500, verbose=0)
    kmeans.fit(data)
    pd_all['des_kmeans5_'+str(k)] = pd_all['description'].map(lambda x : kmeans.predict(tfidf.transform([x]))[0])
    # print pd_all['des_kmeans_'+str(k)]
    from collections import Counter
    print Counter(pd_all['des_kmeans5_'+str(k)])

pd_all[['listing_id', 'des_kmeans5_5', 'des_kmeans5_10', 'des_kmeans5_15', 'des_kmeans5_20', 'des_kmeans5_25']].to_csv('data/train_description_kmeans5_groups_with_data.csv', encoding='utf-8', index=False, quotechar="\"")

# import ipdb; ipdb.set_trace()
sys.exit(0)
