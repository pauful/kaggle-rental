
import pandas as pd


pd_train = pd.read_json('data/train-formatted.json')
pd_test = pd.read_json('data/test-formatted.json')
id_test = pd_test['listing_id']

from collections import Counter

num_train = pd_train.shape[0]
df_all = pd.concat((pd_train, pd_test), axis=0, ignore_index=True)


from Levenshtein import *

df_all['distance_street'] = df_all.apply(lambda x: distance(x['street_address'],x['display_address']) , axis=1)

print df_all['distance_street']

df_all[['listing_id','distance_street']].to_csv('data/train_levenhstein.csv', index=False)
