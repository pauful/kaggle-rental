import datetime
import pandas as pd
train_df = pd.read_json('data/train-formatted.json')
test_df = pd.read_json('data/test-formatted.json')
num_train = train_df.shape[0]
df = pd.concat([train_df, test_df], 0)

df["created"] = pd.to_datetime(df["created"])

mintime = df['created'].min()



def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0

mintime = unix_time_millis(mintime)

df["millis"] = df["created"].map(lambda x: unix_time_millis(x))

df['bedrooms'] = df['bedrooms'].fillna(-1)
df['bedrooms'] = df['bedrooms'].apply(str)
df['interest_level'] = df['interest_level'].fillna('')
df['interest_level'] = df['interest_level'].apply(str)
df['ib'] = df['bedrooms'] + '*'+ df['interest_level']

df_train = df.iloc[:num_train]

agg_prices_time = (df_train.
      sort_values("millis").
      groupby(['ib',"millis"]).
      agg(sum)['price'].
      reset_index("millis"))

print agg_prices_time
agg_prices_time.to_csv('agg_prices_time.csv')
agg_prices_time = pd.read_csv('agg_prices_time.csv')
import sys
def cc(x, agg, il):
    # print x
    suma =  agg[(agg['ib'] == x.split('#')[0].split('*')[0]+'*'+il)  & (agg_prices_time['millis'] < float(x.split('#')[1])) ].tail(10)['price'].sum()
    count =  agg[(agg['ib'] ==x.split('#')[0].split('*')[0]+'*'+il)  & (agg_prices_time['millis'] < float(x.split('#')[1])) ].tail(10)['price'].count()

    # print suma
    # print count


    return suma / count

df['millis_str'] = df['millis'].apply(str)
# df['bedrooms'] = df['bedrooms'].fillna(-1)
df['mgr_list'] = df['ib'] + '#'+ df['millis_str']

df['bed_list_avg_int_h']=df['mgr_list'].map(lambda x: cc(x,agg_prices_time, 'medium'))
df['bed_list_avg_int_m']=df['mgr_list'].map(lambda x: cc(x,agg_prices_time, 'high'))

# df['ma


df[['listing_id','bed_list_avg_int_h','bed_list_avg_int_m']].to_csv('data/train_acc_bed_price_i.csv', encoding='utf-8', index=False)
