import pandas as pd

pd1 = pd.read_csv('submission.csv')
pd2 = pd.read_csv('submission-0.540367031377.csv')

pdd = pd.merge(pd1, pd2, how='left', on='listing_id')


pdd['lowl']=(pdd['low1']+pdd['low'])/2
pdd['mediuml']=(pdd['medium1']+pdd['medium'])/2
pdd['highl']=(pdd['high1']+pdd['high'])/2

pdd[['listing_id','lowl','mediuml','highl']].to_csv('submission-merge.csv', index=False)
